import React, {Component} from 'react';
import {Dimensions, View} from 'react-native';
import {Router, Stack, Scene, Drawer, Actions} from 'react-native-router-flux'
import Sidebar from '../sidebar'
import Home from '../screens/home'
import Login from '../screens/Login'
import Confirmation from '../screens/confirmation'
import Profile from '../screens/profile'
import UserInfo from '../screens/userInfo'
import UserOrders from '../screens/userOrders'
import Reminders from '../screens/reminders'
import Wallet from '../screens/wallet'
import InsuranceBuy from '../screens/insuranceBuy'
import VacleInfo from '../screens/vacleInfo'
import Prices from '../screens/prices'
import CustomerInfo from '../screens/customerInfo'
import PictureUpload from '../screens/pictureUpload'
import TimeSelecting from '../screens/timeSelecting'
import FinalConfirm from '../screens/finalConfirm'
import Payment from '../screens/payment'
import LatestNews from '../screens/latestNews'
import ImportantNews from '../screens/importantNews'
import BlogDetail from '../screens/blogDetail'
import About from '../screens/about'
import Rules from '../screens/rules'
import Splash from '../screens/splash'
import {store} from '../config/store';
import {Provider, connect} from 'react-redux';
import Register from "../screens/Register";
import ScalingDrawer from '../components/react-native-scaling-drawer'
import Configure from '../screens/configure'
import OtherVacleInfo from '../screens/otherVacleInfo'
import AddReminder from '../screens/addReminder'
import Marketing from '../screens/marketing'
import Refferer from '../screens/refferer'
import Yy from '../screens/yy'

const onBackAndroid = () => {
    return Actions.pop();
};
let defaultScalingDrawerConfig = {
    scalingFactor: 0.5,
    minimizeFactor: 0.5,
    swipeOffset: 10
};
class MainDrawerNavigator extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }
    // componentWillReceiveProps(nextProps) {
    //     /** Active Drawer Swipe **/
    //     if (nextProps.navigation.state.index === 0)
    //         this._drawer.blockSwipeAbleDrawer(false);
    //
    //     if (nextProps.navigation.state.index === 0 && this.props.navigation.state.index === 0) {
    //         this._drawer.blockSwipeAbleDrawer(false);
    //         this._drawer.close();75
    //     }
    //
    //     /** Block Drawer Swipe **/
    //     if (nextProps.navigation.state.index > 0) {
    //         this._drawer.blockSwipeAbleDrawer(true);
    //     }
    // }

    setDynamicDrawerValue = (type, value) => {
        defaultScalingDrawerConfig[type] = value;
        /** forceUpdate show drawer dynamic scaling example **/
        this.forceUpdate();
    };
    render() {
        const RouterWidthRedux = connect()(Router);
        return (
                <Provider store={store} openDrawer={() =>this._drawer.open()} closeDrawer={() =>this._drawer.close()} nava>
                    <ScalingDrawer
                        content={<Sidebar openDrawer={() =>this._drawer.open()} closeDrawer={() =>this._drawer.close()} />}
                        ref={ref => this._drawer = ref}
                        {...defaultScalingDrawerConfig}
                        onClose={() => console.log('close')}
                        onOpen={() => console.log('open')}
                    >
                    <RouterWidthRedux openDrawer={() =>this._drawer.open()} closeDrawer={() =>this._drawer.close()} navBar={Yy}>
                        <Scene>
                            {/*<Scene key="root">*/}
                            {/*<Scene key="Splash" component={Splash} initial hideNavBar/>*/}
                            {/*</Scene>*/}
                            {/*<Stack key="container" >*/}
                                <Scene key="login" component={Login} title="Login" />
                                <Scene key="confirmation" component={Confirmation} title="Confirmation" />
                                <Scene key="register" component={Register} title="Register" />
                                <Scene key="home" component={Home} title="Home"  />
                                <Scene key="profile"  component={Profile} title="Profile" />
                                <Scene key="userInfo" component={UserInfo} title="UserInfo" />
                                <Scene key="userOrders" component={UserOrders} title="UserOrders" />
                                <Scene key="reminders" component={Reminders} title="Reminders" />
                                <Scene key="wallet" component={Wallet} title="Wallet" />
                                <Scene key="insuranceBuy" initial component={InsuranceBuy} title="InsuranceBuy"  />
                                <Scene key="vacleInfo" component={VacleInfo} title="VacleInfo" />
                                <Scene key="prices" component={Prices} title="Prices" />
                                <Scene key="customerInfo" component={CustomerInfo} title="CustomerInfo" />
                                <Scene key="pictureUpload" component={PictureUpload} title="PictureUpload" />
                                <Scene key="timeSelecting" component={TimeSelecting} title="TimeSelecting" />
                                <Scene key="finalConfirm" component={FinalConfirm} title="FinalConfirm" />
                                <Scene key="payment" component={Payment} title="Payment" />
                                <Scene key="latestNews" component={LatestNews} title="LatestNews" />
                                <Scene key="importantNews" component={ImportantNews} title="ImportantNews" />
                                <Scene key="blogDetail" component={BlogDetail} title="BlogDetail" />
                                <Scene key="about"  component={About} title="About" />
                                <Scene key="rules" component={Rules} title="Rules" />
                                <Scene key="configure" component={Configure} title="Configure" />
                                <Scene key="otherVacleInfo" component={OtherVacleInfo} title="OtherVacleInfo" />
                                <Scene key="addReminder" component={AddReminder} title="AddReminder" />
                                <Scene key="marketing" component={Marketing} title="Marketing" />
                                <Scene key="refferer"  component={Refferer} title="Refferer" />
                            {/*</Stack>*/}
                        </Scene>
                    </RouterWidthRedux>
                    </ScalingDrawer>
                </Provider>


        );
    }
}
export default MainDrawerNavigator;



