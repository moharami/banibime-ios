import React, {Component} from 'react';
import {View, TouchableOpacity, ScrollView, Text, SafeAreaView, AsyncStorage, BackHandler, Keyboard, Image, TextInput, TouchableHighlight} from 'react-native';
import styles from './styles'
import {Actions} from 'react-native-router-flux'
import IIcon from 'react-native-vector-icons/dist/Ionicons';
import FIcon from 'react-native-vector-icons/dist/Feather';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import {store} from '../../config/store';
import {connect} from 'react-redux';
import Axios from 'axios';
export const url = 'https://banibime.com/api/v1';
Axios.defaults.baseURL = url;
import Loader from '../../components/loader'
import HomeHeader from "../../components/homeHeader/index";
import vacle1 from "../../assets/vacle/vacle1.png";
import vacle2 from "../../assets/vacle/vacle2.png";
import vacle3 from "../../assets/vacle/vacle3.png";
import SwitchRow from '../../components/switchRow'
import moment_jalaali from 'moment-jalaali'
import PersianCalendarPicker from 'react-native-persian-calendar-picker';
import SearchableDropdown from 'react-native-searchable-dropdown';
import AlertView from '../../components/modalMassage'
import ModalFilterPicker from 'react-native-modal-filter-picker'
import Selectbox from 'react-native-selectbox'
import SelectInput from 'react-native-select-input-ios'
import LinearGradient from 'react-native-linear-gradient'

class VacleInfo extends Component {
    constructor(props){
        super(props);
        this.state={
            loading:false,
            loading2:false,
            loading3:false,
            status:1,
            activeButton:7.7,
            activeVacle:0,
            activeVacleName:null,
            showPicker:false,
            selectedStartDate:null,
            modalVisible: false,
            type:"",
            name:0,
            nameTitle:null,
            year:0,
            yearTitle:null,
            damageNumber:0,
            damageNumberTitle:null,
            damageYear:0,
            damageYearTitle:null,
            timeOf:0,
            nextStep: false,
            usage:0,
            usageTitle:null,
            offs:0,
            bodyThirdBime:0,
            bodyThirdBimeTitle:null,
            bodyBime:0,
            bodyBimeTitle:null,
            vaclePrice:"",
            carCats:[],
            kindCats:[],
            modelCats:[],
            vacleType:0,
            vacleTypeTitle:null,
            Chemical:false,
            break_glass:false,
            natural_disaster:false,
            Steal:false,
            Price:false,
            Transit:false,
            transfer:false,
            feature:[],
            vacleId: null,
            filter: 'point-down',
            insurances: [],
            redBorder: false,
            toolTipVisible: true,
            visibleName: false,
            visibleModel: false,
        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress() {
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    componentWillMount(){
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
        // this.setState({loading: true});
        this.setState({
            loading: true
        }, () => {
            Axios.get('/request/get-cat-cars').then(response => {
                this.setState({carCats: response.data.data});
                if(this.props.motor){
                    let data = [];
                    data = response.data.data;
                    data.map((item) => {
                        if(item.name === "موتورسیکلت" ) {
                            this.setState({
                                vacleId: item.id
                            }, () => {
                                this.getCat(item.id);
                            })
                        }
                    })
                }
                Axios.get('/request/get_insurance').then(response => {
                    this.setState({loading: false, insurances: response.data.data});
                })
                    .catch((error) =>{
                        // Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
                        // this.setState({loading: false});
                        this.setState({modalVisible: true, loading: false});

                    });
                console.log('cat cars', response.data.data);
            })
                .catch((error) =>{
                        // Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
                        // this.setState({loading: false});
                        this.setState({modalVisible: true, loading: false});

                    }
                );
        })
        if(this.props.motor){
            this.getCat(this.state.vacleId);
        }
    }
    test(itemValue) {
        if(this.props.thirdBime) {
            if(this.props.motor === false) {
                if( this.state.activeVacle !== 0 && this.state.name !== 0  && this.state.vacleType !== 0 && this.state.usage !== 0  && this.state.selectedStartDate !== null) {
                    this.setState({ nextStep: true });
                }
            }
            else if(this.props.motor === true) {
                if( this.state.name !== 0  && this.state.vacleType !== 0 && this.state.selectedStartDate !== null) {
                    this.setState({ nextStep: true });
                }
            }
            else {
                this.setState({ nextStep: false})
            }
        }
        if(this.props.bodyBime) {
            if( this.state.activeVacle !== 0 && this.state.name !== 0  && this.state.vacleType !== 0 && this.state.vaclePrice !== "") {
                this.setState({ nextStep: true });
            }
            else {
                this.setState({ nextStep: false})
            }
        }
    }
    onDateChange(date) {
        setTimeout(() => {this.setState({showPicker: false})}, 200)
        this.setState({ selectedStartDate: date }, ()=> { this.test();});
    }
    nextStep() {
        if(this.state.nextStep) {
            // AsyncStorage.getItem('token').then((info) => {
            //     const newInfo = JSON.parse(info);
            //     const token = newInfo.token;
            //     const id = newInfo.user_id;
            let user_details = {
                _token:null,
                user_id: null,
                fname:null,
                lname:null,
                national_id:null,
                birthday:null,
                state:null,
                city:null,
                tel:null,
                post_code:null,
                address:null,
                reciver:null,
                new_state:null,
                new_city:null,
                new_tel:null,
                new_post_code:null,
                new_address:null,
                new_reciver:null,
                mobile:null,
                new_mobile:null,
                date_0:null,
                date_1:null,
                time_delivery:null,
                date_2:null,
                address_selected:null,
                lat:null,
                lng:null,
            };
            if(this.props.bodyBime) {
                let factor = {
                    user_id:null,
                    body_car_cate:this.state.activeVacle,
                    body_car_name:this.state.name,
                    body_car_model:this.state.vacleType,
                    body_car_years:this.state.year,
                    body_car_price:this.state.vaclePrice,
                    insurance_id:null,
                    chemical:this.state.Chemical,
                    break_glass:this.state.break_glass,
                    natural_disaster:this.state.natural_disaster,
                    steal:this.state.Steal,
                    price:this.state.Price,
                    Transit:this.state.Transit,
                    transfer:this.state.transfer,
                    third_dmg_years:this.state.bodyThirdBime,
                    body_dmg_years:this.state.bodyBime,
                    sort:this.state.filter,
                    before:null,
                    one:null,
                    two:null,
                    dataSelect:{
                        body_car_cate:this.state.activeVacleName,
                        body_car_name:this.state.nameTitle,
                        body_car_model:this.state.vacleTypeTitle,
                        body_car_years:this.state.yearTitle,
                        third_dmg_years:this.state.bodyThirdBimeTitle,
                        body_dmg_years:this.state.bodyBimeTitle,
                        feature: this.state.feature,
                        fianl_price:null
                    }
                };
                this.setState({loading:true});
                Axios.post('/request/req_body', {
                    body_car_cate:this.state.activeVacle,
                    body_car_name:this.state.name,
                    body_car_model:this.state.vacleType,
                    body_car_years:this.state.year,
                    third_dmg_years:this.state.bodyThirdBime ,
                    body_dmg_years:this.state.bodyBime,
                    body_car_price:this.state.vaclePrice,
                    insurance_id:null,
                    Chemical:this.state.Chemical,
                    break_glass:this.state.break_glass,
                    natural_disaster:this.state.natural_disaster,
                    Steal:this.state.Steal,
                    Price:this.state.Price,
                    Transit:this.state.Transit,
                    transfer:this.state.transfer,
                    sort: this.state.filter
                }).then(response=> {
                    this.setState({loading: false});
                    // console.log(response.data.data)
                    // if(this.state.feature !== null) {
                    //     let str = "";
                    //     this.state.feature.map((item)=>  str = str + item +", ")
                    //     factor.dataSelect.feature = str;
                    // }
                    console.log('body respponse', response.data.data)
                    Actions.prices({openDrawer:this.props.openDrawer, bime: Object.values(response.data.data), pageTitle: 'قیمت ها', factor:factor, user_details:user_details, insurType: 'body' , Chemical:this.state.Chemical,
                        break_glass:this.state.break_glass,
                        natural_disaster:this.state.natural_disaster,
                        Steal:this.state.Steal,
                        Price:this.state.Price,
                        Transit:this.state.Transit,
                        transfer:this.state.transfer})
                })
                    .catch((error) => {
                        console.log(error)
                        console.log(error.response)
                        //  Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
                        // this.setState({loading: false});
                        this.setState({modalVisible: true, loading: false});

                    });
            }
            else if(this.props.thirdBime || this.props.motor) {
                let stttr =  moment_jalaali(this.state.selectedStartDate).format('jYYYY/jM/jD').replace(/\//gi, '-')
                stttr = stttr.replace(/۰/g, "0");
                stttr = stttr.replace(/۱/g, "1");
                stttr = stttr.replace(/۲/g, "2");
                stttr = stttr.replace(/۳/g, "3");
                stttr = stttr.replace(/۴/g, "4");
                stttr = stttr.replace(/۵/g, "5");
                stttr = stttr.replace(/۶/g, "6");
                stttr = stttr.replace(/۷/g, "7");
                stttr = stttr.replace(/۸/g, "8");
                stttr = stttr.replace(/۹/g, "9");
                console.log('neW strrr  ', stttr )
                let factor = {
                    user_id:null,
                    expiredDate: stttr,
                    insurance_id: null,
                    third_car_cate: this.props.motor ? this.state.vacleId :this.state.activeVacle,
                    third_car_model: this.state.vacleType,
                    third_car_name: this.state.name,
                    third_car_years: this.state.year,
                    third_dmg_count: this.state.damageNumber,
                    third_dmg_years: this.state.damageYear,
                    usein: this.props.motor ? null : this.state.usage,
                    commitments: this.state.activeButton,
                    sort:this.state.filter,
                    before:null,
                    one:null,
                    two:null,
                    dataSelect:{
                        third_car_cate:this.state.activeVacleName,
                        third_car_model:this.state.vacleTypeTitle,
                        third_car_name:this.state.nameTitle,
                        third_car_years:this.state.yearTitle,
                        third_dmg_count: this.state.damageNumberTitle,
                        third_dmg_years: this.state.damageYearTitle,
                        third_car_uses:this.state.usageTitle,
                        commitments:this.state.activeButton,
                        feature:null,
                        fianl_price:null
                    }
                };
                this.setState({loading:true});

                // console.log('expiredDate', moment(this.state.selectedStartDate).unix())

                console.log('third_car_cate',this.state.activeVacle)
                console.log('third_car_model',this.state.vacleType)
                console.log('third_car_name',this.state.name)
                console.log('third_car_years',this.state.year)
                console.log('third_dmg_count',this.state.damageNumber)
                console.log('third_dmg_years',this.state.damageYear)
                console.log('usein',this.state.usage)
                console.log('commitments',this.state.activeButton)
                console.log('jjjjjjjjjdate', moment_jalaali(this.state.selectedStartDate).format('jYYYY/jM/jD').replace(/\//gi, '-'))
                Axios.post('/request/req_third_person', {
                    expiredDate: stttr,
                    insurance_id: null,
                    third_car_cate: this.state.activeVacle,
                    third_car_model: this.state.vacleType,
                    third_car_name: this.state.name,
                    third_car_years: this.state.year,
                    third_dmg_count: this.state.damageNumber,
                    third_dmg_years: this.state.damageYear,
                    usein: this.state.usage,
                    commitments: this.state.activeButton,
                    sort: this.state.filter
                }).then(response=> {
                    this.setState({loading: false});
                    console.log('third bime',response.data.data)
                    console.log('insurType:this.props.motor ? motorThird lllllllllllllllllllllllllllll', this.props.motor ?'motor':'Third')

                    Actions.prices({openDrawer:this.props.openDrawer, bime: Object.values(response.data.data), pageTitle: 'قیمت ها', factor:factor, user_details:user_details, insurType:this.props.motor ?'motor':'Third'})
                })
                    .catch((response) => {
                        console.log(response.response)
                        // Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
                        // this.setState({loading: false});
                        this.setState({modalVisible: true, loading: false});
                    });
            }
        }
        else{
            this.setState({redBorder: true})
        }
    }
    getCat(id) {
        console.log(id);
        let idNew = this.props.motor? this.state.vacleId : id;

        this.setState({loading3:true});
        Axios.get('/request/get-kind-cars/'+idNew).then(response => {
            this.setState({loading3: false, kindCats: response.data.data});
            console.log('kind cars', response.data.data);
        })
            .catch((error) =>{
                //  Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
                // this.setState({loading: false});
                this.setState({modalVisible: true, loading3: false});

            });
    }
    getModel(id) {
        console.log(id);
        this.setState({loading2:true});
        Axios.get('/request/get-model-cars/'+id).then(response => {
            this.setState({loading2: false, modelCats: response.data.data});
            console.log('model cars', response.data.data);
        })
            .catch((error) =>{
                // Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
                // this.setState({loading2: false});
                this.setState({modalVisible: true, loading2: false});

            });
    }
    switchButtons(status, id, title) {
        console.log(status)
        if(id === 1) {
            this.setState({
                Chemical: status
            }, () => {
                if(status) {
                    let feature = this.state.feature;
                    feature.push(title)
                    this.setState({feature: feature});
                }
            })
        }
        else if(id === 2) {
            this.setState({
                break_glass: status
            }, () => {
                if(status) {
                    let feature = this.state.feature;
                    feature.push(title)
                    this.setState({feature: feature});
                }
            })
        }
        else if(id === 3) {
            this.setState({
                natural_disaster: status
            }, () => {
                if(status) {
                    let feature = this.state.feature;
                    feature.push(title)
                    this.setState({feature: feature});
                }
            })
        }
        else if(id === 4) {
            this.setState({
                Steal: status
            }, () => {
                if(status) {
                    let feature = this.state.feature;
                    feature.push(title)
                    this.setState({feature: feature});
                }
            })
        }
        else if(id === 5) {
            this.setState({
                Price: status
            }, () => {
                if(status) {
                    let feature = this.state.feature;
                    feature.push(title)
                    this.setState({feature: feature});
                }
            })
        }
        else if(id === 6) {
            this.setState({
                Transit: status
            }, () => {
                if(status) {
                    let feature = this.state.feature;
                    feature.push(title)
                    this.setState({feature: feature});
                }
            })
        }
        else if(id === 7) {
            this.setState({
                transfer: status
            }, () => {
                if(status) {
                    let feature = this.state.feature;
                    feature.push(title)
                    this.setState({feature: feature});
                }
            })
        }
    }
    closeModal() {
        this.setState({modalVisible: false});
    }
    onShowName = () => {
        this.setState({ visibleName: true });
    }

    onSelectName = (picked) => {
        console.log('picked', picked)
        this.setState({
            name: picked,
            visibleName: false
        }, () =>{
            this.state.kindCats.length !== 0 && this.state.kindCats.map((item) => {
                if(picked === item.id) {
                    this.setState({nameTitle: item.name}, () => {console.log(this.state.nameTitle)
                    });
                }
            })
            this.test(picked);
            this.getModel(picked);
        })
    }

    onCancelName = () => {
        this.setState({
            visibleName: false
        });
    }

    onShowModel = () => {
        this.setState({ visibleModel: true });
    }

    onSelectModel = (picked) => {
        console.log('picked', picked)
        this.setState({
            vacleType: picked,
            visibleModel: false
        }, () =>{
            this.state.modelCats.length !== 0 && this.state.modelCats.map((item) => {
                if(picked === item.id) {
                    this.setState({vacleTypeTitle: item.name}, () => {console.log(this.state.vacleTypeTitle)
                    });
                }
            })
            this.test();
        })
    }

    onCancelModel = () => {
        this.setState({
            visibleModel: false
        })
    }



    renderOption = (settings) =>{
        const { item, getLabel } = settings
        return (
            <View style={styles.optionContainer}>
                <View style={styles.innerContainer}>
                    <View style={[styles.box, { backgroundColor: item.color }]} />
                    <Text style={{ color: item.color, alignSelf: 'flex-start' }}>{getLabel(item)}</Text>
                </View>
            </View>
        )
    }

    render() {
        // const now = new Date()
        // alert(moment_jalaali(now))
        const vaclePriceBody = this.state.vaclePrice.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")

        // const {posts, categories, user} = this.props;
        // const text11 =  <Text style={{textAlign: "right", fontFamily: "IRANSansMobile(FaNum)"}} >1971</Text>
        const yearsArray = ["1397 - 2018", "1396 - 2017", "1395 - 2016", "1394 - 2015", "1393 - 2014", "1392 - 2013", "1391 - 2012", "1390 - 2011", "1389 - 2010", "1388 - 2009", "1387 - 2008", "1386 - 2007", "1385 - 2006", "1384 - 2005", "1383 - 2004", "1382 - 2003", "1381 - 2002", "1380 - 2001", "1379 - 2000","1378 - 1999","1377 - 1998","1376 - 1997","1375 - 1996","1374 - 1995","1373 - 1995","1372 - 1994","1371 - 1993","1370 - 1992","1369 - 1991","1368 - 1990","1367 - 1989","1366 - 1988","1364 - 1987","1363 - 1986","1362 - 1985","1361 - 1984","1360 - 1983"];
        const offsArray = ["بدون تخفیف","یک سال تخفیف", "دو سال تخفیف" , "سه سال تخفیف", "چهار سال تخفیف", "پنج سال تخفیف" , "شش سال تخفیف", "هفت سال تخفیف", "هشت سال تخفیف" , "نه سال تخفیف", "ده سال تخفیف", "یازده سال تخفیف" , "دوازده سال تخفیف"];
        const bodyThirdBimeArray = ["بدون تخفیف","یک سال ", "دو سال " , "سه سال ", "چهار سال ", "پنج سال " , "شش سال ", "هفت سال ", "هشت سال " , "نه سال ", "ده سال ", "یازده سال "];
        const dmgNumberArray = [ "بدون خسارت", "یک بار مالی", "دو بار مالی و بیشتر", "یک بار جانی", "یک بار جانی و بیشتر"];
        const usageArray = ["انتخاب نشده", "تاکسی", "شخصی", "درون شهری", "برون شهری", "اداری", "مدارس"];
        // if(this.state.loading){
        //     return (<Loader />)
        // }

        // {
        //     this.state.kindCats.map((item) => {return {key: item.id, label: item.name}}
        //     )
        // }
        let items = [];
        yearsArray.map((item, index) => {items.push({key: index, label: item, value: index})})

        let dmgItems = [];
        dmgNumberArray.map((item, index) => {dmgItems.push({key: index, label: item, value: index})})

        let offsArrayItems = [];
        offsArray.map((item, index) => {offsArrayItems.push({key: index, label: item, value: index})})

        let usageItems = [];
        usageArray.map((item, index) => {usageItems.push({key: index, label: item, value: index})})

        let bodyThirdBimeItems = [];
        bodyThirdBimeArray.map((item, index) => {bodyThirdBimeItems.push({key: index, label: item, value: index})})

        if(this.state.loading){
            return (<Loader />)
        }
        return (
            <SafeAreaView style={styles.container}>
                <View style={styles.header}>
                    <HomeHeader active={1} pageTitle={this.props.pageTitle} openDrawer={this.props.openDrawer}/>
                </View>
                <ScrollView style={styles.scroll}>
                    <View style={styles.bodyContainer}>
                        <View style={{width: "100%", zIndex: 0, paddingBottom: 70}}>
                            {
                                this.props.motor ? null :
                                    <View>
                                        <Text style={styles.text}>نوع وسیله نقلیه</Text>
                                        <View style={[styles.vacleContainer, {borderRadius: 10, borderColor: this.state.redBorder && this.state.activeVacle === 0 ? 'red' : (this.state.activeVacle !== 0 ? 'rgb(50, 197, 117)' : 'transparent'), borderWidth: 1}]}>
                                            {
                                                this.state.carCats.map((item, index) =>
                                                    <TouchableOpacity key={index} style={[styles.vacle, {backgroundColor: this.state.activeVacle === item.id ? 'white': 'transparent', elevation: this.state.activeVacle === item.id ? 4 : 0}]} onPress={()=>{
                                                        this.setState({
                                                            activeVacle: item.id,
                                                            activeVacleName: item.name
                                                        }, () => {
                                                            this.test();
                                                            this.getCat(item.id);
                                                        })}}>
                                                        {
                                                            this.props.bodyBime ? (item.name !== 'موتورسیکلت' ?
                                                                    <Text style={styles.vacleText}>{item.name}</Text> : null

                                                            ) : <Text style={styles.vacleText}>{item.name}</Text>

                                                        }
                                                        {/*<Text style={styles.vacleText}>{item.name}</Text>*/}
                                                        {
                                                            item.name === "موتورسیکلت" ?
                                                                (this.props.bodyBime ? null:  <Image source={vacle3} style={[styles.Image, {tintColor: this.state.activeVacle === item.id ?  "rgb(15, 76, 183)" : undefined}]} />) :
                                                                (
                                                                    item.name ===  "بارکش" ?
                                                                        <Image source={vacle2} style={[styles.Image, {tintColor:  this.state.activeVacle === item.id ?  "rgb(15, 76, 183)" : undefined}]} />:
                                                                        <Image source={vacle1} style={[styles.Image, {tintColor: this.state.activeVacle === item.id ?  "rgb(15, 76, 183)" : "gray"}]} />
                                                                )
                                                        }
                                                    </TouchableOpacity>
                                                )
                                            }
                                        </View>
                                    </View>
                            }
                            <Text style={styles.text}>نام وسیله نقلیه</Text>
                            <TouchableOpacity onPress={this.onShowName} style={{position: 'relative', elevation: 5, zIndex: 3, width: '100%', backgroundColor: 'white', height: 42,  borderColor: this.state.redBorder && this.state.name === 0 ? 'red' : (this.state.name !== 0 ? 'rgb(50, 197, 117)' : 'transparent'), borderWidth: 1, borderRadius: 10}}>
                                <FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: 10, left: 10}}/>
                                <Text style={styles.itemLabel}>{this.state.loading3 ? "در حال بارگذاری..." : this.state.name === 0 ? "انتخاب نشده" : this.state.nameTitle}</Text>
                                <ModalFilterPicker
                                    visible={this.state.visibleName}
                                    onSelect={this.onSelectName}
                                    onCancel={this.onCancelName}
                                    filterTextInputContainerStyle={{padding: 15, borderBottomWidth: 1, borderBottomColor: 'gray'}}
                                    options={ this.state.kindCats.map((item) => {return {key: item.id, label: item.name}})}
                                    placeholderText="جستجو ..."
                                    cancelButtonText="لغو"
                                    filterTextInputStyle={{textAlign: 'right'}}
                                    optionTextStyle={{textAlign: 'right', width: '100%'}}
                                    titleTextStyle={{textAlign: 'right'}}
                                />
                            </TouchableOpacity>
                            <Text style={styles.text}>مدل وسیله نقلیه</Text>
                            <TouchableOpacity onPress={this.onShowModel} style={{position: 'relative', elevation: 5, zIndex: 3, width: '100%', backgroundColor: 'white', height: 42,  borderColor: this.state.redBorder && this.state.vacleType === 0 ? 'red' : (this.state.vacleType !== 0 ? 'rgb(50, 197, 117)' : 'transparent'), borderWidth: 1, borderRadius: 10}}>
                                <FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: 10, left: 10}}/>
                                <Text style={styles.itemLabel}>{this.state.loading ? "در حال بارگذاری..." : this.state.vacleType === 0 ? "انتخاب نشده" : this.state.vacleTypeTitle}</Text>
                                <ModalFilterPicker
                                    visible={this.state.visibleModel}
                                    onSelect={this.onSelectModel}
                                    onCancel={this.onCancelModel}
                                    filterTextInputContainerStyle={{padding: 15, borderBottomWidth: 1, borderBottomColor: 'gray'}}
                                    options={ this.state.modelCats.map((item) => {return {key: item.id, label: item.name}})}
                                    placeholderText="جستجو ..."
                                    cancelButtonText="لغو"
                                    filterTextInputStyle={{textAlign: 'right'}}
                                    optionTextStyle={{textAlign: 'right', width: '100%'}}
                                    titleTextStyle={{textAlign: 'right'}}
                                />
                            </TouchableOpacity>
                            <Text style={styles.text}>سال ساخت</Text>
                            <View style={{position: 'relative', elevation: 5, zIndex: 3, width: '100%'}}>
                                <FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: '45%', left: 10}}/>
                                <SelectInput
                                    value={this.state.year}
                                    options={items}
                                    onCancelEditing={() => console.log('onCancel')}
                                    submitKeyText="انتخاب"
                                    cancelKeyText="لغو"
                                    labelStyle={{color: 'rgb(150, 150, 150)', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13, paddingLeft: '78%'}}
                                    pickerItemsStyle={{fontFamily: 'IRANSansMobile(FaNum)', fontSize: 15}}
                                    buttonsTextStyle={{fontFamily: 'IRANSansMobile(FaNum)', fontSize: 15}}
                                    onSubmitEditing={(itemValue) =>{
                                        console.log('itemValue for years', itemValue)
                                        this.setState({
                                            year: itemValue
                                        }, () => {

                                            this.setState({yearTitle: yearsArray[itemValue]}, () => {console.log(this.state.yearTitle)});
                                            this.test();
                                        })}}
                                    // style={{height: 42, paddingRight: 10, width: '100%', backgroundColor: 'white'}}
                                    style={{
                                        flexDirection: 'row',
                                        height: 42,
                                        padding: 8,
                                        marginTop: 16,
                                        backgroundColor: '#FFFFFF',
                                        borderRadius: 10, borderColor:  (this.state.year !== 0 ? 'rgb(50, 197, 117)' : 'transparent'), borderWidth:  this.state.year !== 0 ? 1 : 0
                                    }}
                                />
                            </View>

                            {
                                this.props.thirdBime ?
                                    <View>
                                        <Text style={styles.text}>تاریخ انقضای بیمه نامه قبلی</Text>

                                        <TouchableOpacity onPress={() => this.setState({showPicker: true})} style={{position: 'relative', elevation: 5, zIndex: 3, width: '100%', backgroundColor: 'white', height: 42,  borderColor: this.state.redBorder && this.state.selectedStartDate === null ? 'red' :  (this.state.selectedStartDate !== null ? 'rgb(50, 197, 117)' : 'transparent'), borderWidth: 1, borderRadius: 10}}>
                                            <Text style={{textAlign: 'right', paddingTop: '3%', paddingRight: 10}}>{this.state.selectedStartDate !==null ? moment_jalaali(this.state.selectedStartDate).format('jYYYY/jM/jD') : 'انتخاب کنید'}</Text>
                                            <Icon name="calendar-o" size={20} color="rgb(61, 99, 224)" style={{position: 'absolute', zIndex: 90, top: 10, left: 10}}/>
                                        </TouchableOpacity>

                                        <Text style={styles.text}>تعداد خسارت</Text>
                                        <View style={{position: 'relative', elevation: 5, zIndex: 3, width: '100%'}}>
                                            <FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: '45%', left: 10}}/>
                                            <SelectInput
                                                value={this.state.damageNumber}
                                                options={dmgItems}
                                                onCancelEditing={() => console.log('onCancel')}
                                                submitKeyText="انتخاب"
                                                cancelKeyText="لغو"
                                                labelStyle={{color: 'rgb(150, 150, 150)', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13, paddingLeft: '78%'}}
                                                pickerItemsStyle={{fontFamily: 'IRANSansMobile(FaNum)', fontSize: 15}}
                                                buttonsTextStyle={{fontFamily: 'IRANSansMobile(FaNum)', fontSize: 15}}
                                                onSubmitEditing={(itemValue) => {
                                                    console.log('itemValue for years', itemValue)
                                                    this.setState({
                                                        damageNumber: itemValue
                                                    }, () => {
                                                        this.setState({damageNumberTitle: dmgNumberArray[itemValue]}, () => {console.log(this.state.damageNumberTitle)});
                                                        this.test();
                                                    })}}
                                                // style={{height: 42, paddingRight: 10, width: '100%', backgroundColor: 'white'}}
                                                style={{
                                                    flexDirection: 'row',
                                                    height: 42,
                                                    padding: 8,
                                                    marginTop: 16,
                                                    backgroundColor: '#FFFFFF',
                                                    borderRadius: 10, borderColor:  (this.state.damageNumber !== 0 ? 'rgb(50, 197, 117)' : 'transparent'), borderWidth:  this.state.damageNumber !== 0 ? 1 : 0
                                                }}
                                            />
                                        </View>

                                        <Text style={styles.text}>سال های عدم خسارت در بیمه قبلی</Text>
                                        <View style={{position: 'relative', elevation: 5, zIndex: 3, width: '100%'}}>
                                            <FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: '45%', left: 10}}/>
                                            <SelectInput
                                                value={this.state.damageYear}
                                                options={offsArrayItems}
                                                onCancelEditing={() => console.log('onCancel')}
                                                submitKeyText="انتخاب"
                                                cancelKeyText="لغو"
                                                labelStyle={{color: 'rgb(150, 150, 150)', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13, paddingLeft: '78%'}}
                                                pickerItemsStyle={{fontFamily: 'IRANSansMobile(FaNum)', fontSize: 15}}
                                                buttonsTextStyle={{fontFamily: 'IRANSansMobile(FaNum)', fontSize: 15}}
                                                onSubmitEditing={(itemValue) =>{
                                                    console.log('itemValue for years', itemValue)
                                                    this.setState({
                                                        damageYear: itemValue
                                                    }, () => {
                                                        this.setState({damageYearTitle: offsArray[itemValue]}, () => {console.log(this.state.damageYearTitle)});
                                                        this.test();
                                                    })}}
                                                // style={{height: 42, paddingRight: 10, width: '100%', backgroundColor: 'white'}}
                                                style={{
                                                    flexDirection: 'row',
                                                    height: 42,
                                                    padding: 8,
                                                    marginTop: 16,
                                                    backgroundColor: '#FFFFFF',
                                                    borderRadius: 10, borderColor:  (this.state.damageYear !== 0 ? 'rgb(50, 197, 117)' : 'transparent'), borderWidth:  this.state.damageYear !== 0 ? 1 : 0
                                                }}
                                            />
                                        </View>

                                        {
                                            this.props.motor ? null :
                                                <View>
                                                    <Text style={styles.text}>کاربری</Text>
                                                    <View style={{position: 'relative', elevation: 5, zIndex: 3, width: '100%'}}>
                                                        <FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: '45%', left: 10}}/>
                                                        <SelectInput
                                                            value={this.state.usage}
                                                            options={usageItems}
                                                            onCancelEditing={() => console.log('onCancel')}
                                                            submitKeyText="انتخاب"
                                                            cancelKeyText="لغو"
                                                            labelStyle={{color: 'rgb(150, 150, 150)', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13, paddingLeft: '78%'}}
                                                            pickerItemsStyle={{fontFamily: 'IRANSansMobile(FaNum)', fontSize: 15}}
                                                            buttonsTextStyle={{fontFamily: 'IRANSansMobile(FaNum)', fontSize: 15}}
                                                            onSubmitEditing={(itemValue) =>{
                                                                console.log('itemValue for years', itemValue)
                                                                this.setState({
                                                                    usage: itemValue
                                                                }, () => {
                                                                    this.setState({usageTitle: usageArray[itemValue]}, () => {console.log(this.state.usageTitle)});
                                                                    this.test();
                                                                })}}
                                                            // style={{height: 42, paddingRight: 10, width: '100%', backgroundColor: 'white'}}
                                                            style={{
                                                                flexDirection: 'row',
                                                                height: 42,
                                                                padding: 8,
                                                                marginTop: 16,
                                                                backgroundColor: '#FFFFFF',
                                                                borderRadius: 10, borderColor: this.state.redBorder && this.state.usage === 0 ? 'red' : (this.state.usage !== 0 ? 'rgb(50, 197, 117)' : 'transparent'), borderWidth: 1
                                                            }}
                                                        />
                                                    </View>

                                                </View>
                                        }
                                        <View style={styles.textRow}>
                                            <Text style={styles.value}>308،000،000 ریال</Text>
                                            <Text style={styles.labelTxt}>خسارت بدنی: </Text>
                                        </View>
                                        <View style={styles.textRow}>
                                            <Text style={styles.value}>7،700،000 ریال</Text>
                                            <Text style={styles.labelTxt}>خسارت مالی: </Text>
                                        </View>
                                        <View style={styles.textRow}>
                                            <Text style={styles.value}>231،000،000 ریال</Text>
                                            <Text style={styles.labelTxt}>خسارت راننده: </Text>
                                        </View>

                                        <Text style={styles.text}>حداکثر پوشش بیمه</Text>

                                        <View style={styles.numberOfMeals}>
                                            <TouchableOpacity onPress={()=>{this.setState({activeButton: 7.7}, () => {this.test();})}}>
                                                <View style={[styles.circle, {backgroundColor: this.state.activeButton === 7.7 ? 'rgb(17, 103, 253)' : 'white'}]}>
                                                    <Text style={[styles.questionText, {fontSize: 14, color: this.state.activeButton === 7.7 ? 'white' : 'black'}]}>7.7</Text>
                                                </View>
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={()=>{this.setState({activeButton: 10}, () => {this.test();})}}>
                                                <View style={[styles.circle, {backgroundColor: this.state.activeButton === 10 ? 'rgb(17, 103, 253)' : 'white'}]}>
                                                    <Text style={[styles.questionText, {fontSize: 14, color: this.state.activeButton === 10 ? 'white' : 'black'}]}>10</Text>
                                                </View>
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={()=>{this.setState({activeButton: 15}, () => {this.test();})}}>
                                                <View style={[styles.circle, {backgroundColor: this.state.activeButton === 15 ? 'rgb(17, 103, 253)' : 'white'}]}>
                                                    <Text style={[styles.questionText, {fontSize: 14, color: this.state.activeButton === 15 ? 'white' : 'black'}]}>15</Text>
                                                </View>
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={()=>{this.setState({activeButton: 20}, () => {this.test();})}}>
                                                <View style={[styles.circle, {backgroundColor: this.state.activeButton === 20 ? 'rgb(17, 103, 253)' : 'white'}]}>
                                                    <Text style={[styles.questionText, {fontSize: 14, color: this.state.activeButton === 20 ? 'white' : 'black' }]}>20</Text>
                                                </View>
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={()=>{this.setState({activeButton: 25}, () => {this.test();})}}>
                                                <View style={[styles.circle, {backgroundColor: this.state.activeButton === 25 ? 'rgb(17, 103, 253)' : 'white'}]}>
                                                    <Text style={[styles.questionText, {fontSize: 14, color: this.state.activeButton === 25 ? 'white' : 'black'}]}>25</Text>
                                                </View>
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={()=>{this.setState({activeButton: 30}, () => {this.test();})}}>
                                                <View style={[styles.circle, {backgroundColor: this.state.activeButton === 30 ? 'rgb(17, 103, 253)' : 'white'}]}>
                                                    <Text style={[styles.questionText, {fontSize: 14, color: this.state.activeButton === 30 ? 'white' : 'black'}]}>30</Text>
                                                </View>
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={()=>{this.setState({activeButton: 35}, () => {this.test();})}}>
                                                <View style={[styles.circle, {backgroundColor: this.state.activeButton === 35 ? 'rgb(17, 103, 253)' : 'white'}]}>
                                                    <Text style={[styles.questionText, {fontSize: 14, color: this.state.activeButton === 35 ? 'white' : 'black'}]}>35</Text>
                                                </View>
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={()=>{this.setState({activeButton: 40}, () => {this.test();})}}>
                                                <View style={[styles.circle, {backgroundColor: this.state.activeButton === 40 ? 'rgb(17, 103, 253)' : 'white'}]}>
                                                    <Text style={[styles.questionText, {fontSize: 14, color: this.state.activeButton === 40 ? 'white' : 'black'}]}>40</Text>
                                                </View>
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={()=>{this.setState({activeButton: 45}, () => {this.test();})}}>
                                                <View style={[styles.circle, {backgroundColor: this.state.activeButton === 45 ? 'rgb(17, 103, 253)' : 'white'}]}>
                                                    <Text style={[styles.questionText, {fontSize: 14, color: this.state.activeButton === 45 ? 'white' : 'black'}]}>45</Text>
                                                </View>
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={()=>{this.setState({activeButton: 50}, () => {this.test();})}}>
                                                <View style={[styles.circle, {backgroundColor: this.state.activeButton === 50 ? 'rgb(17, 103, 253)' : 'white'}]}>
                                                    <Text style={[styles.questionText, {fontSize: 14, color: this.state.activeButton === 50 ? 'white' : 'black'}]}>50</Text>
                                                </View>
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={()=>{this.setState({activeButton: 60}, () => {this.test();})}}>
                                                <View style={[styles.circle, {backgroundColor: this.state.activeButton === 60 ? 'rgb(17, 103, 253)' : 'white'}]}>
                                                    <Text style={[styles.questionText, {fontSize: 14, color: this.state.activeButton === 60 ? 'white' : 'black'}]}>60</Text>
                                                </View>
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={()=>{this.setState({activeButton: 70}, () => {this.test();})}}>
                                                <View style={[styles.circle, {backgroundColor: this.state.activeButton === 70 ? 'rgb(17, 103, 253)' : 'white'}]}>
                                                    <Text style={[styles.questionText, {fontSize: 14, color: this.state.activeButton === 70 ? 'white' : 'black'}]}>70</Text>
                                                </View>
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={()=>{this.setState({activeButton: 80}, () => {this.test();})}}>
                                                <View style={[styles.circle, {backgroundColor: this.state.activeButton === 80 ? 'rgb(17, 103, 253)' : 'white'}]}>
                                                    <Text style={[styles.questionText, {fontSize: 14, color: this.state.activeButton === 80 ? 'white' : 'black'}]}>80</Text>
                                                </View>
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={()=>{this.setState({activeButton: 90}, () => {this.test();})}}>
                                                <View style={[styles.circle, {backgroundColor: this.state.activeButton === 90 ? 'rgb(17, 103, 253)' : 'white'}]}>
                                                    <Text style={[styles.questionText, {fontSize: 14, color: this.state.activeButton === 90 ? 'white' : 'black'}]}>90</Text>
                                                </View>
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={()=>{this.setState({activeButton: 100}, () => {this.test();})}}>
                                                <View style={[styles.circle, {backgroundColor: this.state.activeButton === 100 ? 'rgb(17, 103, 253)' : 'white'}]}>
                                                    <Text style={[styles.questionText, {fontSize: 14, color: this.state.activeButton === 100 ? 'white' : 'black'}]}>100</Text>
                                                </View>
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={()=>{this.setState({activeButton: 120}, () => {this.test();})}}>
                                                <View style={[styles.circle, {backgroundColor: this.state.activeButton === 120 ? 'rgb(17, 103, 253)' : 'white'}]}>
                                                    <Text style={[styles.questionText, {fontSize: 14, color: this.state.activeButton === 120 ? 'white' : 'black'}]}>120</Text>
                                                </View>
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={()=>{this.setState({activeButton: 140}, () => {this.test();})}}>
                                                <View style={[styles.circle, {backgroundColor: this.state.activeButton === 140 ? 'rgb(17, 103, 253)' : 'white'}]}>
                                                    <Text style={[styles.questionText, {fontSize: 14, color: this.state.activeButton === 140 ? 'white' : 'black'}]}>140</Text>
                                                </View>
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={()=>{this.setState({activeButton: 154}, () => {this.test();})}}>
                                                <View style={[styles.circle, {backgroundColor: this.state.activeButton === 154 ? 'rgb(17, 103, 253)' : 'white'}]}>
                                                    <Text style={[styles.questionText, {fontSize: 14, color: this.state.activeButton === 154 ? 'white' : 'black'}]}>154</Text>
                                                </View>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                    : null
                            }
                            {
                                this.props.bodyBime ?
                                    <View>
                                        <Text style={styles.text}>سال های عدم خسارت در شخص ثالث</Text>


                                        <View style={{position: 'relative', elevation: 5, zIndex: 3, width: '100%'}}>
                                            <FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: '45%', left: 10}}/>
                                            <SelectInput
                                                value={this.state.bodyThirdBime}
                                                options={bodyThirdBimeItems}
                                                onCancelEditing={() => console.log('onCancel')}
                                                submitKeyText="انتخاب"
                                                cancelKeyText="لغو"
                                                labelStyle={{color: 'rgb(150, 150, 150)', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13, paddingLeft: '78%'}}
                                                pickerItemsStyle={{fontFamily: 'IRANSansMobile(FaNum)', fontSize: 15}}
                                                buttonsTextStyle={{fontFamily: 'IRANSansMobile(FaNum)', fontSize: 15}}
                                                onSubmitEditing={(itemValue) =>{
                                                    console.log('itemValue for years', itemValue)
                                                    this.setState({
                                                        bodyThirdBime: itemValue
                                                    }, () => {
                                                        this.setState({bodyThirdBimeTitle: bodyThirdBimeArray[itemValue]}, () => {console.log(this.state.bodyThirdBimeTitle)});
                                                        this.test();
                                                    })}}
                                                // style={{height: 42, paddingRight: 10, width: '100%', backgroundColor: 'white'}}
                                                style={{
                                                    flexDirection: 'row',
                                                    height: 42,
                                                    padding: 8,
                                                    marginTop: 16,
                                                    backgroundColor: '#FFFFFF',
                                                    borderRadius: 10, borderColor:  (this.state.bodyThirdBime !== 0 ? 'rgb(50, 197, 117)' : 'transparent'), borderWidth:  this.state.bodyThirdBime !== 0 ? 1 : 0
                                                }}
                                            />
                                        </View>

                                        <Text style={styles.text}>سال های عدم خسارت در بیمه بدنه</Text>


                                        <View style={{position: 'relative', elevation: 5, zIndex: 3, width: '100%'}}>
                                            <FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: '45%', left: 10}}/>
                                            <SelectInput
                                                value={this.state.bodyBime}
                                                options={bodyThirdBimeItems}
                                                onCancelEditing={() => console.log('onCancel')}
                                                submitKeyText="انتخاب"
                                                cancelKeyText="لغو"
                                                labelStyle={{color: 'rgb(150, 150, 150)', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13, paddingLeft: '78%'}}
                                                pickerItemsStyle={{fontFamily: 'IRANSansMobile(FaNum)', fontSize: 15}}
                                                buttonsTextStyle={{fontFamily: 'IRANSansMobile(FaNum)', fontSize: 15}}
                                                onSubmitEditing={(itemValue) =>{
                                                    console.log('itemValue for years', itemValue)
                                                    this.setState({
                                                        bodyBime: itemValue
                                                    }, () => {
                                                        this.setState({bodyBimeTitle: bodyThirdBimeArray[itemValue]}, () => {console.log(this.state.bodyBimeTitle)});
                                                        this.test();
                                                    })}}
                                                style={{
                                                    flexDirection: 'row',
                                                    height: 42,
                                                    padding: 8,
                                                    marginTop: 16,
                                                    backgroundColor: '#FFFFFF',
                                                    borderRadius: 10, borderColor:  (this.state.bodyBime !== 0 ? 'rgb(50, 197, 117)' : 'transparent'), borderWidth:  this.state.bodyBime !== 0 ? 1 : 0
                                                }}
                                            />
                                        </View>
                                        <Text style={styles.text}>ارزش خودرو(تومان)</Text>
                                        <View style={{position: 'relative', elevation: 5, zIndex: 3, width: '100%', backgroundColor: 'white', height: 42, borderRadius: 10, borderColor: this.state.redBorder && this.state.vaclePrice === '' ? 'red' : (this.state.vaclePrice !== '' ? 'rgb(50, 197, 117)' : 'transparent'), borderWidth: this.state.redBorder && this.state.vaclePrice === '' ? 1 : this.state.vaclePrice !== '' ? 1 : 0}}>
                                            <TextInput
                                                onChangeText={(text) =>{
                                                    this.setState({
                                                        vaclePrice: text.toString().replace(/,/g, "")
                                                    }, () => {
                                                        this.test();
                                                    })}}
                                                placeholder='ارزش خودرو'
                                                keyboardType={"numeric"}
                                                placeholderTextColor={'gray'}
                                                underlineColorAndroid='transparent'
                                                // value={this.state.vaclePrice}
                                                value={vaclePriceBody}
                                                secureTextEntry={this.props.secure}
                                                style={{
                                                    height: 42,
                                                    paddingRight: 25,
                                                    width: '100%',
                                                    color: 'gray',
                                                    fontSize: 14,
                                                    textAlign: 'right',
                                                    // elevation: 4,
                                                    fontFamily: 'IRANSansMobile(FaNum)',
                                                    shadowColor: 'lightgray',
                                                    shadowOffset: {width: 5,height: 5},
                                                    shadowOpacity: 1
                                                }}
                                            />
                                        </View>
                                        <Text style={styles.featureLabel}>نحوه پرداخت</Text>
                                        <SwitchRow label="پرداخت نقدی" switchButtons={(status) => this.switchButtons(status, 10, 'chemical')} />
                                        <Text style={styles.featureLabel}>مشخصات خودرو</Text>
                                        <SwitchRow label="خوودرو صفر" switchButtons={(status) => this.switchButtons(status, 8, 'chemical')}  />
                                        <SwitchRow label="خودرو وارداتی" switchButtons={(status) => this.switchButtons(status, 9, 'chemical')} />
                                        <Text style={styles.featureLabel}>پوشش های اجباری</Text>
                                        <SwitchRow label="آتش سوزی" switchButtons={() => null} on />
                                        <SwitchRow label="صاعقه" switchButtons={() => null} on />
                                        <SwitchRow label="انفجار" switchButtons={() => null} on />
                                        <SwitchRow label="حادثه" switchButtons={() => null} on />
                                        <SwitchRow label="سرقت کلی" switchButtons={() => null} on />
                                        <Text style={styles.featureLabel}>پوشش های اختیاری</Text>
                                        <SwitchRow label="پاشیدن مواد شیمیایی و اسیدی" switchButtons={(status) => this.switchButtons(status, 1, 'chemical')} />
                                        <SwitchRow label="شکست شیشه" switchButtons={(status) => this.switchButtons(status, 2, 'break_glass')}/>
                                        <SwitchRow label="سیل، زلزله و بلایای طبیعی" switchButtons={(status) => this.switchButtons(status, 3, 'natural_disaster')} />
                                        <SwitchRow label="سرقت درجا" switchButtons={(status) => this.switchButtons(status, 4, 'steal')} />
                                        <SwitchRow label="نوسانات ارزش بازار" switchButtons={(status) => this.switchButtons(status, 5, 'price')} />
                                        <SwitchRow label="ایاب و ذهاب" switchButtons={(status) => this.switchButtons(status, 6, 'transit')} />
                                        <SwitchRow label="خروج از کشور" switchButtons={(status) => this.switchButtons(status, 7, 'transfer')} />
                                    </View> : null
                            }
                        </View>
                        {
                            this.state.showPicker ?
                                <View style={{
                                    position: 'absolute',
                                    bottom: '56%',
                                    zIndex: 9999,
                                    backgroundColor: 'white'
                                }}>
                                    <PersianCalendarPicker
                                        onDateChange={(date) => this.onDateChange(date)}
                                    />
                                </View>
                                : null
                        }
                        <AlertView
                            closeModal={(title) => this.closeModal(title)}
                            modalVisible={this.state.modalVisible}
                            onChange={(visible) => this.setModalVisible(visible)}

                            title='مشکلی در برقراری ارتباط با سرور به وجود آمده لطفا دوباره سعی کنید'
                        />
                    </View>
                </ScrollView>
                <View style={styles.footer}>
                    <TouchableOpacity style={[styles.iconLeftContainer, {backgroundColor: this.state.nextStep ? 'rgba(255, 193, 39, 1)' : 'rgba(200, 200, 200, 1)' }]} onPress={() => this.nextStep()}>
                        <FIcon name="arrow-left" size={18} color="white" style={{borderColor: 'white', borderWidth: 1, borderRadius: 20, padding: 3, marginRight: 5}} />
                        <Text style={styles.label}>بعدی</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.iconRightContainer} onPress={() => this.onBackPress()}>
                        <IIcon name="md-close" size={22} color="rgba(17, 103, 253, 1)" />
                    </TouchableOpacity>
                </View>
                <LinearGradient  start={{x: 0, y: 0}} end={{x: 1, y: 1}} colors={['rgba(60, 177, 232, 1)', 'rgba(62, 64, 219, 1)']} style={styles.topBlue} />
            </SafeAreaView>
        );
    }
}
export default VacleInfo;
