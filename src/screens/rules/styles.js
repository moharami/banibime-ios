
import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    container: {
        flex: 1,
        // position: 'relative',
        // zIndex: 1,
        backgroundColor: 'rgb(246, 246, 246)',
        // backgroundColor: 'red'
    },
    header: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        backgroundColor: 'white',
        height: 60,
        // position: 'absolute',
        // top: 0,
        // right: 0,
        // left: 0,
        // zIndex: 9990
    },
    bodyContainer: {
        // paddingBottom: 180,
        paddingRight: 10,
        paddingLeft: 10,
        // paddingTop: 30
    },
    scroll: {
        // paddingTop: 60,
        paddingRight: 15,
        paddingLeft: 15,
        paddingBottom: '120%',
        position: 'absolute',
        top: '14%',
        bottom: '6%',
        right: 0,
        left: 0,
        zIndex: 9990
    },
    topLabel: {
        paddingLeft: 15,
        paddingRight: 10,
        color: 'black',
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 18,
    },
    labelContainer: {
        flexDirection: 'row',
        alignItems: "center",
        justifyContent: 'flex-end',
        paddingBottom: 20
    },
    body: {
        flexDirection: 'row-reverse',
        flexWrap: 'wrap'
    },
    linearcontainer: {
        flex: 1,
        height: 60,
        position: 'absolute',
        top: 0,
        right: 0,
        left: 0,
        zIndex: 9999,
        borderBottomColor: 'lightgray',
        borderBottomWidth: 1,
        paddingRight: 15,
        paddingLeft: 15
    },
    top: {
        flexDirection: 'row',
        alignItems: "center",
        justifyContent: 'space-between',
        paddingTop: 15,
        // paddingBottom: 30
    },
    headerTitle: {
        color: 'white',
        fontSize: 16,
        fontFamily: 'IRANSansMobile(FaNum)',
        // paddingRight: '32%'
    },
    name: {
        color: 'gray',
        fontSize: 12,
        fontFamily: 'IRANSansMobile(FaNum)',
    },
    topBlue: {
        backgroundColor: 'rgb(61, 99, 223)',
        width: '100%',
        height: 45,
        position: 'absolute',
        top: 0,
        right: 0,
        left: 0,
        zIndex: -1,
    },

});