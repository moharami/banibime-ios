import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    container: {
        flex: 1,
        // position: 'relative'
    },
    image: {
        width: '100%',
        height: '20%',
        // height: '36%'
        // resizeMode: 'contain',
        backgroundColor: 'rgb(61, 99, 223)',
    },
    TrapezoidStyle: {
        flex: 1,
        position: 'absolute',
        bottom: '78%',
        right: 0,
        left: 0,
        zIndex: 100,

        borderTopWidth: 50,
        borderLeftWidth: 0,
        borderBottomWidth: 0,
        borderTopColor: 'rgb(61, 99, 223)',
        borderRightColor: 'rgb(233, 233, 233)',
        borderLeftColor: 'transparent',
        borderBottomColor: 'transparent',
    },
    topBlue: {
        backgroundColor: 'rgb(61, 99, 223)',
        width: '100%',
        height: 60,
        position: 'absolute',
        top: 0,
        right: 0,
        left: 0,
        zIndex: -1,
    },
    imageRow: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'flex-start',
        justifyContent: 'space-between',
        // paddingTop: 20
    },
    profileImage: {
        width: 85,
        height: 85,
        borderRadius: 85,
    },
    profile: {
        // width: 90,
        // marginRight: '30%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        paddingLeft: 30,
        paddingBottom: 7,
        paddingTop: 20

    },
    imageContainer: {
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 20,
        borderRadius: 100,
        elevation: 8
    },
    name: {
        fontSize: 13,
        color: 'rgb(255, 255, 255)',
        fontFamily: 'IRANSansMobile(FaNum)',
        textAlign: 'center',
        paddingTop: 10,
        paddingLeft: 20,
        paddingBottom: 5
    },
    item: {
        width: '28%',
        height: '60%'
    },
    bodyImage: {
        width: 50,
        resizeMode: 'contain'
    },
    bodyyImage: {
        width: 50,
        resizeMode: 'contain'
    },

    scroll: {
        position: 'absolute',
        flex: 1,
        top: '10%',
        right: 0,
        left: 0,
        bottom: 0,
        zIndex: 9997,
        marginBottom: 60
    },
    body : {
        // position: 'absolute',
        // flex: 1,
        alignItems: 'center',
        justifyContent: 'flex-start',
        // top: 120,
        // right: 0,
        // left: 0,
        // zIndex: 9998,
        paddingRight: 20,
        paddingLeft: 20,
        paddingBottom: 20,
// backgroundColor: 'rgba(11, 11, 11, .2)',


        position: 'absolute',
        flex: 1,
        top: '20%',
        right: 0,
        left: 0,
        // bottom: 0,
        zIndex: 9999,
        // marginBottom: 60

    },
    row: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        // paddingBottom: 10
    },
    lastrow: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        // paddingBottom: 20,
        // paddingRight: '10%',
        // paddingLeft: '10%',
    },
    iconContainer: {
        width: 30,
        height: 30,
        borderRadius: 30,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
    },
    label: {
        fontSize: 12,
        fontFamily: 'IRANSansMobile(FaNum)',
        textAlign: 'center',
        paddingTop: 10
    },
});
