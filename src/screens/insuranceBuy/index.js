import React, {Component} from 'react';
import { View, TouchableOpacity, Text, DeviceInfo,SafeAreaView, Image, BackHandler, NetInfo, Dimensions, AsyncStorage, Platform} from 'react-native';
import styles from './styles'
import {Actions} from 'react-native-router-flux';
import background from '../../assets/bg.png'
import Axios from 'axios'
;
export const url = 'https://banibime.com/api/v1';
Axios.defaults.baseURL = url;
import Loader from '../../components/loader'
import ProfileItem from "../../components/profileItem/index";
import Icon from 'react-native-vector-icons/dist/Feather';
import FooterMenu from "../../components/footerMenu/index";
import AlertView from "../../components/modalMassage";
import bime1 from "../../assets/bime/1.png";
import bime2 from "../../assets/bime/2.png";
import bime3 from "../../assets/bime/3.png";
import bime4 from "../../assets/bime/4.png";
import bime5 from "../../assets/bime/5.png";
import bime6 from "../../assets/bime/6.png";
import bime7 from "../../assets/bime/7.png";
import motor from "../../assets/motor.png";

import {connect} from 'react-redux';
import {store} from '../../config/store';
class InsuranceBuy extends Component {
    constructor(props){
        super(props);
        this.state = {
            text: '',
            login: true,
            loading: false,
            modalVisible: false,
            wifi: false,
            isConnected: false
        };
    }
    componentWillUnmount() {
        console.log('some thing unmount')
        BackHandler.addEventListener("hardwareBackPress",  this.onBackPress);
        // BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress() {
        console.log('some thing loged')
        BackHandler.exitApp();
        // Actions.pop({refresh: {refresh:Math.random()}});
        // return true;
    };
    componentWillUpdate(){
        BackHandler.addEventListener("hardwareBackPress",  this.onBackPress.bind(this));
    }
    componentWillMount() {
         // AsyncStorage.removeItem('token')
        // console.log('is nuuuuumber',/^[-]?\d+$/.test('08555fyfhfghf'))
        // BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
        NetInfo.getConnectionInfo().then((connectionInfo) => {
            this.setState({isConnected: connectionInfo.type !== 'none'}, ()=> {
                if(this.state.isConnected === false){
                    this.setState({modalVisible: true, loading: false, wifi: true})
                }
            })
        });
        console.log('heere is insBuy', DeviceInfo.getModel)
        AsyncStorage.getItem('token').then((info) => {
            if(info !== null) {
                const newInfo = JSON.parse(info);
                console.log('iiiiiiiiinfo', newInfo)
                this.setState({loading: true})
                Axios.post('/user', {
                    mobile: newInfo.mobile
                }).then(response=> {
                    this.setState({loading: false});

                    console.log('profile', newInfo.mobile)
                    store.dispatch({type: 'USER_INFO_FETCHED', payload: response.data.data});
                    this.setState({loading: false})
                })
                .catch((error) => {
                    // Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
                    // this.setState({loading: false});
                    this.setState({modalVisible: true, loading: false});
                });
            }
        });
    }
    closeModal() {
        this.setState({modalVisible: false});
    }
    render() {
        const {user} = this.props;
        if(this.state.loading){
            return (<Loader />)
        }
        else return (
            <SafeAreaView style={styles.container}>
                <View style={styles.image}>
                    <View style={styles.imageRow}>
                        <View style={styles.profile}>
                            {/*<View style={styles.iconContainer}>*/}
                                {/*<Icon name="user" size={20} />*/}
                            {/*</View>*/}
                            {/*<Text style={styles.name}>{user !== null ? user.fname : ''} {user !== null ? user.lname : ''}</Text>*/}
                        </View>
                        <Image style={{marginTop: 15,resizeMode: 'contain' , marginRight: '5%', marginBottom: 40}}
                               source={require('../../assets/logo/loaderLogo.png')}/>
                        <TouchableOpacity onPress={() => this.props.openDrawer()}>
                            <Icon name="menu" size={30} color="white" style={{paddingRight: 15, paddingTop: 20}} />
                        </TouchableOpacity>
                    </View>
                </View>
                {/*<ScrollView style={styles.scroll}>*/}
                    <View style={styles.body}>
                        <View style={styles.row}>
                            <View style={styles.item}>
                                <TouchableOpacity onPress={() => Actions.otherVacleInfo({openDrawer: this.props.openDrawer, fire: true, pageTitle: 'بیمه آتش سوزی'})} style={styles.imageContainer}>
                                    <Image source={bime5} style={styles.bodyImage}  />
                                </TouchableOpacity>
                                <Text style={styles.label}>بیمه آتش سوزی</Text>
                            </View>
                            <View style={styles.item}>
                                <TouchableOpacity style={styles.imageContainer} onPress={() => Actions.vacleInfo({openDrawer: this.props.openDrawer, bodyBime: true, pageTitle: 'بیمه بدنه خودرو'})}>
                                    <Image source={bime6} style={styles.bodyImage} />
                                </TouchableOpacity>
                                <Text style={styles.label}>بیمه بدنه خودرو</Text>
                            </View>
                            <View style={styles.item}>
                                <TouchableOpacity style={styles.imageContainer} onPress={() => Actions.vacleInfo({openDrawer: this.props.openDrawer, thirdBime: true,motor: false, pageTitle: 'بیمه شخص ثالث'})}>
                                    <Image source={bime1} style={styles.bodyImage} />
                                </TouchableOpacity>
                                <Text style={styles.label}>بیمه شخص ثالث</Text>
                            </View>
                        </View>
                        <View style={styles.row}>
                            <View style={styles.item}>
                                <TouchableOpacity onPress={() => Actions.otherVacleInfo({openDrawer: this.props.openDrawer, individual: true, pageTitle: 'بیمه درمان انفرادی'})} style={styles.imageContainer}>
                                    <Image source={bime3} style={styles.bodyImage} />
                                </TouchableOpacity>
                                <Text style={styles.label}>بیمه درمان انفرادی</Text>
                            </View>
                            <View style={styles.item}>
                                <TouchableOpacity onPress={() => Actions.otherVacleInfo({openDrawer: this.props.openDrawer, medical: true, pageTitle: 'بیمه مسئولیت پزشکی'})} style={styles.imageContainer}>
                                    <Image source={bime2} style={styles.bodyImage} />
                                </TouchableOpacity>
                                <Text style={styles.label}>بیمه مسئولیت پزشکی</Text>
                            </View>
                            <View style={styles.item}>
                                <TouchableOpacity onPress={() => Actions.otherVacleInfo({openDrawer: this.props.openDrawer, age: true, pageTitle: 'بیمه عمر'})} style={styles.imageContainer}>
                                    <Image source={bime4} style={styles.bodyImage} />
                                </TouchableOpacity>
                                <Text style={styles.label}>بیمه عمر</Text>
                            </View>
                        </View>
                        <View style={styles.lastrow}>
                            <View style={styles.item}>
                                <TouchableOpacity onPress={() => Actions.otherVacleInfo({openDrawer: this.props.openDrawer, travel: true, pageTitle: 'بیمه مسافرتی'})}  style={styles.imageContainer}>
                                    <Image source={bime7} style={styles.bodyImage} />
                                </TouchableOpacity>
                                <Text style={styles.label}>بیمه مسافرتی</Text>
                            </View>
                            <View style={styles.item}>
                                <TouchableOpacity style={[styles.imageContainer, { paddingTop: 30, paddingBottom: 30}]} onPress={() => Actions.vacleInfo({openDrawer: this.props.openDrawer, thirdBime: true, motor: true, pageTitle: 'بیمه موتور سیکلت'})}>
                                    <Image source={motor} style={[styles.bodyImage, {tintColor: 'rgb(85, 113, 221)'}]} />
                                </TouchableOpacity>
                                <Text style={styles.label}>بیمه موتور سیکلت</Text>
                            </View>
                            {/*<View style={styles.item}>*/}
                                {/*<View style={styles.imageContainer}>*/}
                                    {/*<Image source={bime7} style={styles.bodyImage} />*/}
                                {/*</View>*/}
                                {/*<Text style={styles.label}>بیمه مسئولیت مشاغل</Text>*/}
                            {/*</View>*/}
                        </View>
                        <AlertView
                            closeModal={(title) => this.closeModal(title)}
                            modalVisible={this.state.modalVisible}
                            onChange={(visible) => this.setModalVisible(visible)}
                            title={(this.state.wifi ? 'لطفا وضعیت وصل بودن به اینترنت را بررسی کنید': 'مشکلی در برقراری ارتباط با سرور به وجود آمده لطفا دوباره سعی کنید')}
                        />
                    </View>
                {/*</ScrollView>*/}
                <FooterMenu active="bime" openDrawer={this.props.openDrawer}/>
                <View style={[styles.TrapezoidStyle, {borderRightWidth: Dimensions.get('window').width}]} />
                <View style={styles.topBlue} />
            </SafeAreaView>

        );
    }
}
// export default InsuranceBuy;
function mapStateToProps(state) {
    return {
        user: state.auth.user,

    }
}
export default connect(mapStateToProps)(InsuranceBuy);