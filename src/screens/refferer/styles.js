
import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    container: {
        flex: 1,
        position: 'relative'
    },
    header: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    image: {
        width: '100%',
        height: '36%'
        // resizeMode: 'contain'
    },
    imageRow: {
        flexDirection: 'row',
        alignItems: 'flex-start',
        justifyContent: 'space-between',
        paddingTop: 20,
        padding: 15
    },
    profileImage: {
        width: 85,
        height: 85,
        borderRadius: 85,
    },
    profile: {
        width: 90,
        marginRight: '30%'
    },
    label: {
        fontSize: 15,
        color: 'rgb(255, 255, 255)',
        fontFamily: 'IRANSansMobile(FaNum)',
        textAlign: 'center',
    },
    scroll: {
        position: 'absolute',
        flex: 1,
        top: 80,
        right: 0,
        left: 0,
        bottom: 0,
        zIndex: 9999,
        marginBottom: 90
    },
    body : {
        // paddingTop: '38%',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 20,
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
});
