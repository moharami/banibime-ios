import React, {Component} from 'react';
import {View, TouchableOpacity, ScrollView, Text, StatusBar, SafeAreaView, AsyncStorage, BackHandler, Alert, TextInput} from 'react-native';
import styles from './styles'
import {Actions} from 'react-native-router-flux'
import IIcon from 'react-native-vector-icons/dist/Ionicons';
import FIcon from 'react-native-vector-icons/dist/Feather';
import {store} from '../../config/store';
import {connect} from 'react-redux';
import Axios from 'axios';
import LinearGradient from 'react-native-linear-gradient';
export const url = 'https://banibime.com/api/v1';
Axios.defaults.baseURL = url;
import Loader from '../../components/loader'
import HomeHeader from "../../components/homeHeader/index";
import SwitchRow from '../../components/switchRow';
import Slider from "react-native-slider";
import Selectbox from 'react-native-selectbox'
import AlertView from '../../components/modalMassage'
import ModalFilterPicker from 'react-native-modal-filter-picker'
import SelectInput from 'react-native-select-input-ios'

class OtherVacleInfo extends Component {
    constructor(props){
        super(props);
        this.state = {
            loading: false,
            loading2: false,
            input1: 0,
            input1Title:null,
            input2: this.props.medical ? '0': 0,
            input2Title:null,
            input3: 0,
            input3Title:null,
            input4: 0,
            input4Title:null,
            inputTxt1: '',
            inputTxt2: '',
            inputTxt3: '',
            earthquake: false,
            pipe: false,
            earth: false,
            rain: false,
            tornado: false,
            flood: false,
            airplane: false,
            steal: false,
            area_price: 1,
            sliderChange: false,
            countries: [],
            age: this.props.travel ? 0 : 7,
            ageTitle:null,
            expert: [],
            feature: [],
            filter: 'point-down',
            modalVisible: false,
            redBorder: false,
            visibleinput1: false,
            visibleinput2: false
        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress() {
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    componentWillMount(){
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
        // this.setState({loading: true});

            if(this.props.travel) {
                this.setState({loading: true});
                Axios.get('/request/get-country').then(response => {
                    console.log('coun', response.data.data)
                    this.setState({loading: false, countries: response.data.data});
                })
                .catch((error) => {
                    // Alert.alert('','خطا')
                    // this.setState({loading: false});
                    this.setState({modalVisible: true, loading: false});

                    console.log(error);
                });
            }
            // else if(this.props.medical) {
            //     this.setState({loading: true});
            //     Axios.get('/request/get-expert').then(response => {
            //         this.setState({loading: false, expert: response.data.data});
            //     })
            //         .catch((error) => {
            //             // Alert.alert('','خطا')
            //             // this.setState({loading: false});
            //             this.setState({modalVisible: true, loading: false});
            //
            //             console.log(error);
            //         });
            // }
    }
    test() {
        if(this.props.fire) {
            if( this.state.input1 !== 0 && this.state.input2 !== 0  && this.state.inputTxt1 !== ''&& this.state.inputTxt2 !== '' && this.state.inputTxt3 !== '') {
                this.setState({ nextStep: true });
            }
            else {
                this.setState({ nextStep: false})
            }
        }
        if(this.props.travel) {
            if( this.state.input1 !== 0 && this.state.input2 !== 0  && this.state.input3 !== 0 && this.state.age !== 7) {
                this.setState({ nextStep: true });
            }
            else {
                this.setState({ nextStep: false})
            }
        }
        if(this.props.medical) {
            if( this.state.input1 !== 0 && this.state.input2 !== '0'  && this.state.input3 !== 0 && this.state.input4 !== 0) {
                this.setState({ nextStep: true });
            }
            else {
                this.setState({ nextStep: false})
            }
        }
        if(this.props.individual) {
            if(this.state.input1 === 2) {
                if(this.state.input1 !== 0 && this.state.input4 !== 0  && this.state.input2 !== 0 && this.state.age !== 7) {
                    this.setState({ nextStep: true });
                }
                else {
                    this.setState({ nextStep: false})
                }
            }
            else if(this.state.input1 === 1) {
                if (this.state.input1 !== 0 && this.state.input4 !== 0 && this.state.inputTxt1) {
                    this.setState({ nextStep: true });

                }
                else {
                    this.setState({ nextStep: false})
                }
            }
            else {
               this.setState({ nextStep: false})
            }
        }
        if(this.props.age) {
            if(this.state.input1 !== 0 && this.state.input2 !== 0  && this.state.input3 !== 0) {
                this.setState({ nextStep: true });
            }
            else {
                this.setState({ nextStep: false})
            }
        }
    }
    closeModal(title) {
        this.setState({modalVisible: false, type: title});
    }
    nextStep() {
        if(this.state.nextStep ) {
            this.setState({loading: true});
            // AsyncStorage.getItem('token').then((info) => {
                // const newInfo = JSON.parse(info);
                // const token = newInfo.token;
                // const id = newInfo.user_id;
                let user_details = {
                    user_id: null,
                    _token: null,
                    fname:null,
                    lname:null,
                    national_id:null,
                    birthday:null,
                    state:null,
                    city:null,
                    tel:null,
                    post_code:null,
                    address:null,
                    reciver:null,
                    new_state:null,
                    new_city:null,
                    new_tel:null,
                    new_post_code:null,
                    new_address:null,
                    new_reciver:null,
                    mobile: null,
                    new_mobile:null,
                    date_0: null,
                    date_1:null,
                    time_delivery:null,
                    date_2:null,
                    address_selected:null,
                    lat:null,
                    lng:null
                };
                if(this.props.fire) {
                    let factor = {
                        user_id: null,
                        fire_home_type: this.state.input1,
                        fire_structure: this.state.input2,
                        fire_home_count: this.state.inputTxt2,
                        fire_home_price: this.state.inputTxt1,
                        fire_meters: this.state.inputTxt3,
                        insurance_id: null,
                        earthquake: this.state.earthquake,
                        pipe: this.state.pipe,
                        earth: this.state.earth,
                        rain: this.state.rain,
                        tornado: this.state.tornado,
                        flood: this.state.flood,
                        airplane: this.state.airplane,
                        steal: this.state.steal,
                        area_price: this.state.area_price,
                        sort:this.state.filter,
                        before:null,
                        one:null,
                        two:null,
                        dataSelect:{
                            fire_home_type: this.state.input1Title,
                            fire_structure: this.state.input2Title,
                            fire_home_count: this.state.inputTxt2,
                            fire_home_price: this.state.inputTxt1,
                            fire_meters: this.state.inputTxt3,
                            feature: this.state.feature,
                            fianl_price: null
                        }
                    };
                    Axios.post('/request/req_fire', {
                        fire_home_type: this.state.input1,
                        fire_home_count: this.state.inputTxt2,
                        fire_structure: this.state.input2,
                        fire_home_price: this.state.inputTxt1,
                        fire_meters: this.state.inputTxt3,
                        insurance_id: null,
                        earthquake: this.state.earthquake,
                        pipe: this.state.pipe,
                        earth: this.state.earth,
                        rain: this.state.rain,
                        tornado: this.state.tornado,
                        flood: this.state.flood,
                        airplane: this.state.airplane,
                        steal: this.state.steal,
                        area_price: this.state.area_price,
                        sort: this.state.filter
                    }).then(response=> {
                        this.setState({loading: false});
                        // if(this.state.feature !== null) {
                        //     let str = "";
                        //     this.state.feature.map((item)=>  str = str + item +", ")
                        //     factor.dataSelect.feature = str;
                        // }
                        console.log('fire response',response.data.data)
                        Actions.prices({openDrawer: this.props.openDrawer, bime: Object.values(response.data.data), pageTitle: 'قیمت ها', factor: factor, user_details: user_details, insurType: 'fire', earthquake: this.state.earthquake,
                            pipe: this.state.pipe,
                            earth: this.state.earth,
                            rain: this.state.rain,
                            tornado: this.state.tornado,
                            flood: this.state.flood,
                            airplane: this.state.airplane,
                            steal: this.state.steal})
                    })
                    .catch((error) => {
                        console.log(error)
                        // Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
                        // this.setState({loading: false});
                        this.setState({modalVisible: true, loading: false});

                    });
                }
                if(this.props.travel) {
                    let factor = {
                        user_id: null,
                        travel_country: this.state.input1,
                        travel_type: this.state.input2,
                        travel_age: this.state.age,
                        travel_time: this.state.input3,
                        insurance_id:null,
                        area_price:null,
                        price:null,
                        sort:this.state.filter,
                        before:null,
                        one:null,
                        two:null,
                        insurance:{
                            id:null,
                            user_id:null,
                            title:null,
                            description:null,
                            content:null,
                            punishment_danger:null,
                            punishment_off:null,
                            punishment_old:null,
                            punishment_vat:null,
                            tax:null,
                            vat:null,
                            off:null,
                            slug:null,
                            custom_danger:null,
                            extra_life:null,
                            person_danger:null,
                            round_it:null,
                            power:null,
                            branches:null,
                            customer_rate:null,
                            delay:null,
                            center_rank:null,
                            summary_rank:null,
                            shop_share:null,
                            complain_rank:null,
                            display:null,
                            deleted_at:null,
                            created_at:null,
                            updated_at:null,
                            attachments:[]
                        },
                        dataSelect:{
                            insurance:{
                                id:null,
                                user_id:null,
                                title:null,
                                description:null,
                                content:null,
                                punishment_danger:null,
                                punishment_off:null,
                                punishment_old:null,
                                punishment_vat:null,
                                tax:null,
                                vat:null,
                                off:null,
                                slug:null,
                                custom_danger:null,
                                extra_life:null,
                                person_danger:null,
                                round_it:null,
                                power:null,
                                branches:null,
                                customer_rate:null,
                                delay:null,
                                center_rank:null,
                                summary_rank:null,
                                shop_share:null,
                                complain_rank:null,
                                display:null,
                                deleted_at:null,
                                created_at:null,
                                updated_at:null,
                                attachments:[]
                            },
                            travel_age:this.state.ageTitle,
                            travel_time:this.state.input3Title,
                            travel_type:this.state.input2Title,
                            travel_country:this.state.input1Title,
                            fianl_price: null
                        }
                    };
                    console.log('travel_age', this.state.age);
                    console.log('travel_type', this.state.input2);
                    console.log('travel_time', this.state.input3);
                    console.log('travel_country', this.state.input1);

                    Axios.post('/request/req_travel', {
                        travel_country: this.state.input1,
                        travel_type: this.state.input2,
                        travel_age: this.state.age,
                        travel_time: this.state.input3,
                        sort: this.state.filter
                    }).then(response=> {
                        this.setState({loading: false});
                        console.log('travel response',response.data.data)
                        Actions.prices({openDrawer: this.props.openDrawer, bime: response.data.data, pageTitle: 'قیمت ها', factor: factor,  user_details: user_details, insurType: 'travel'})
                    })
                        .catch((response) => {
                            console.log(response.response)
                            // Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
                            // this.setState({loading: false});
                            this.setState({modalVisible: true, loading: false});

                        });
                }
                if(this.props.medical) {
                    let factor = {
                        user_id:null,
                        res_cost:this.state.input1,
                        res_job:this.state.input3,
                        res_time:this.state.input4,
                        resp_type_text: this.state.input2,
                        res_cost_text:null,
                        res_job_text:null,
                        res_time_text: null,
                        record_selected:null,
                        price:null,
                        insurance_id:null,
                        sort:this.state.filter,
                        before:null,
                        one:null,
                        two:null,
                        dataSelect:{
                            res_cost_text:null,
                            res_job_text:null,
                            res_time_text:null,
                            res_cost:this.state.input1Title,
                            res_job:this.state.input3Title,
                            res_time:this.state.input4Title,
                            resp_type_text:this.state.input2,
                            record_selected:null,
                            insurance_id:null,
                            fianl_price:null
                        }
                    };
                    Axios.post('/request/req_responsible', {
                        res_cost: this.state.input1,
                        res_job: this.state.input3,
                        res_time: this.state.input4,
                        resp_type_text: this.state.input2,
                        sort: this.state.filter
                    }).then(response=> {
                        this.setState({loading: false});
                        console.log('travel response',response.data.data);
                        Actions.prices({openDrawer: this.props.openDrawer, bime: response.data.data, pageTitle: 'قیمت ها', factor: factor, user_details: user_details, insurType:'responsible'  })
                    })
                    .catch((error) => {
                        console.log(error)
                        // Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
                        // this.setState({loading: false});
                        this.setState({modalVisible: true, loading: false});

                    });
                }
                if(this.props.individual) {
                    let factor = {
                        user_id: null,
                        age: this.state.input1 === 2 ? this.state.age : 1,
                        type: this.state.input1 === 2 ? this.state.input2 : 1,
                        insurancer: this.state.input4,
                        count_men: this.state.inputTxt1,
                        resp_type: this.state.input1,
                        insurance_id: null,
                        complete_selected_id: null,
                        sort:this.state.filter,
                        before:null,
                        one:null,
                        two:null,
                        dataSelect:{
                            age: this.state.input1 === 2 ? this.state.age : 1,
                            type: this.state.input1 === 2 ? this.state.input2 : 1,
                            insurancer: this.state.input4Title,
                            resp_type: this.state.input1Title,
                            fianl_price: null
                        }
                    }
                    Axios.post('/request/req_complete', {
                        age: this.state.input1 === 2 ? this.state.age : 1,
                        type: this.state.input1 === 2 ? this.state.input2 : 1,
                        insurancer: this.state.input4,
                        count_men: this.state.inputTxt1,
                        resp_type: this.state.input1,
                        sort: this.state.filter
                    }).then(response=> {
                        this.setState({loading: false});
                        console.log('req_complete response',response.data.data);
                        Actions.prices({openDrawer: this.props.openDrawer, bime: response.data.data, pageTitle: 'قیمت ها', factor:factor, user_details: user_details, insurType:'complete'})
                    })
                    .catch((error) => {
                        console.log(error.response)
                        // Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
                        // this.setState({loading: false});
                        this.setState({modalVisible: true, loading: false});
                    });
                }
                if(this.props.age) {
                    let factor = {
                        user_id: null,
                        life_cost: this.state.input1,
                        life_pay: this.state.input2,
                        life_years: this.state.input3,
                        record_selected: null,
                        price: null,
                        insurance_id: null,
                        sort:this.state.filter,
                        before:null,
                        one:null,
                        two:null,
                        insurance:{
                            id:null,
                            user_id:null,
                            title:null,
                            description:null,
                            content:null,
                            punishment_danger:null,
                            punishment_off:null,
                            punishment_old:null,
                            punishment_vat:null,
                            tax:null,
                            vat:null,
                            off:null,
                            slug:null,
                            custom_danger:null,
                            extra_life:null,
                            person_danger:null,
                            round_it:null,
                            power:null,
                            branches:null,
                            customer_rate:null,
                            delay:null,
                            center_rank:null,
                            summary_rank:null,
                            shop_share:null,
                            complain_rank:null,
                            display:null,
                            deleted_at:null,
                            created_at:null,
                            updated_at:null,
                            attachments:[]
                        },
                        dataSelect:{
                            life_cost: this.state.input1Title,
                            life_pay: this.state.input2Title,
                            life_years: this.state.input3Title,
                            feature: null,
                            fianl_price: null
                        }
                    };
                    Axios.post('/request/req_life', {
                        life_cost: this.state.input1,
                        life_pay: this.state.input2,
                        life_years: this.state.input3,
                        sort: this.state.filter
                    }).then(response=> {
                        this.setState({loading: false});
                        console.log('age response',response.data.data)
                        Actions.prices({openDrawer: this.props.openDrawer, bime: response.data.data, pageTitle: 'قیمت ها', factor: factor,  user_details: user_details, insurType:'life' })
                    })
                    .catch((response) => {
                        console.log(response.data.data)
                        // Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
                        // this.setState({loading: false});
                        this.setState({modalVisible: true, loading: false});

                    });
                }
        }
        else {
            this.setState({redBorder: true});
        }
    }
    closeModal() {
        this.setState({modalVisible: false});
    }
    switchButtons(status, id, title) {
        console.log(status)
        if(id === 1) {
            this.setState({
                earthquake: status
            }, () => {
                if(status) {
                    let feature = this.state.feature;
                    feature.push(title)
                    this.setState({feature: feature});
                }
            })
        }
        else if(id === 2) {
            this.setState({
                pipe: status
            }, () => {
                if(status) {
                    let feature = this.state.feature;
                    feature.push(title)
                    this.setState({feature: feature});
                }
            })
        }
        else if(id === 3) {
            this.setState({
                earth: status
            }, () => {
                if(status) {
                    let feature = this.state.feature;
                    feature.push(title)
                    this.setState({feature: feature});
                }
            })
        }
        else if(id === 4) {
            this.setState({
                rain: status
            }, () => {
                if(status) {
                    let feature = this.state.feature;
                    feature.push(title)
                    this.setState({feature: feature});
                }
            })
        }
        else if(id === 5) {
            this.setState({
                tornado: status
            }, () => {
                if(status) {
                    let feature = this.state.feature;
                    feature.push(title)
                    this.setState({feature: feature});
                }
            })
        }
        else if(id === 6) {
            this.setState({
                flood: status
            }, () => {
                if(status) {
                    let feature = this.state.feature;
                    feature.push(title)
                    this.setState({feature: feature});
                }
            })
        }
        else if(id === 7) {
            this.setState({
                airplane: status
            }, () => {
                if(status) {
                    let feature = this.state.feature;
                    feature.push(title)
                    this.setState({feature: feature});
                }
            })
        }
        else if(id === 8) {
            this.setState({
                steal: status
            }, () => {
                if(status) {
                    let feature = this.state.feature;
                    feature.push(title)
                    this.setState({feature: feature});
                }
            })
        }
    }
    onShowinput1 = () => {
        this.setState({ visibleinput1: true });
    }

    onSelectInput1 = (picked) => {
        console.log('picked', picked)
        this.setState({
            input1: picked,
            visibleinput1: false
        }, () =>{
            console.log('ppppicked', picked)
            this.state.countries.map((item) => {
                if(picked === item.id) {
                    this.setState({input1Title: item.title}, () => {console.log('iiiinput 1',this.state.input1)});
                }
            })
            this.test();
        })
    }

    onCancelinput1 = () => {
        this.setState({
            visibleinput1: false
        })
    }

    onShowinput2 = () => {
        this.setState({ visibleinput2: true });
    }

    onSelectInput2 = (picked) => {
        console.log('picked', picked)
        this.setState({
            input1: picked,
            visibleinput2: false
        }, () =>{
            console.log('ppppicked', picked)
            this.state.expert.map((item) => {
                if(picked === item.id) {
                    this.setState({input1Title: item.title}, () => {console.log(this.state.input1Title)});
                }
            })
            this.test();
        })
    }
    onCancelinput2 = () => {
        this.setState({
            visibleinput2: false
        })
    }
    getExperts() {
        const label = this.state.input2;
        console.log('label send', this.state.input2)
        this.setState({loading2: true});
        Axios.post('/request/expertise', {id: this.state.input2}).then(response => {
            this.setState({loading2: false, expert: response.data});
        })
            .catch((error) => {
                this.setState({modalVisible: true, loading2: false});
                console.log(error);
            });
    }
    render() {
        const vaclePriceStr = this.state.inputTxt1.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

        const typeArray = ["انتخاب نشده", "یک واحد در آپارتمان", "یک ساختمان ویلایی", "آپارتمان یا مجتمع"];
        const structureArray = ["انتخاب نشده", "آجری", "فلزی", "بتنی"];

        const payArray = ["انتخاب نشده", "12,750,000 تومان", "8,500,000 تومان", "5,300,000 تومان", "3,200,000 تومان", "1,600,000 تومان", "1,050,000 تومان"];
        const costArray = ["انتخاب نشده", "ماهیانه", "سه ماهه", "شش ماهه", "سالانه"];
        const yearArray = ["انتخاب نشده", "13 ساله", "16 ساله", "20 ساله", "25 ساله"];
        const medicalNumArray = ["انتخاب نشده", "یک دیه", "دو دیه", "سه دیه"];
        const medicalTimeArray = ["انتخاب نشده", "سه ماهه", "شش ماهه", "یک ساله"];

        const indivTypeArray = ["انتخاب نشده", "تامین اجتماعی", "نیرو های مسلح", "بانک ها"];
        const insurancerIndivTimeArray = ["انتخاب نشده", "انفرادی", "گروهی"];

        const insuranceTypeArray = ["انتخاب نشده", "10,000 یورو (آسیا آفریقا بجز چین و ژاپن)", " 30,000 یورو", "50,000 یورو (شینگن و سایر کشورها بجز چین ، ژاپن ،آمریکا ،کانادا و استرالیا", "50,000 یورو (کلیه کشورها )"];
        const travelArray = ["انتخاب نشده", "1 تا 7 روز", "8 تا 15 روز", "16 تا 23 روز", "24 تا 31 روز", "32 تا 45 روز", "46 تا 62 روز", "63 تا 92 روز", "6 ماه", "1 سال"];

        const ageArray = ["انتخاب نشده", "1-12", "13-65", "66-70", "71-75", "76-80", "81-85"];



        let typeArrayItem = [];
        typeArray.map((item, index) => {typeArrayItem.push({key: index, label: item, value: index})})

        let structureArrayItem = [
            {key: 0, label: "انتخاب نشده", value: 0},
            {key: 1, label: "آجری", value: "bricks"},
            {key: 2, label: "فلزی", value: "metal"},
            {key: 3, label: "بتنی", value: "concrete"},
        ];
        // structureArray.map((item, index) => {structureArrayItem.push({key: index, label: item, value: index})})

        let payArrayItem = [];
        payArray.map((item, index) => {payArrayItem.push({key: index, label: item, value: index})})

        let costArrayItem = [];
        costArray.map((item, index) => {costArrayItem.push({key: index, label: item, value: index})})

        let yearArrayItem = [
            {key: 0, label: "انتخاب نشده", value: 0},
            {key: 1, label: "13 ساله", value: 13 },
            {key: 2, label: "16 ساله", value: 16},
            {key: 3, label: "20 ساله", value: 20},
            {key: 4, label: "25 ساله", value: 25},
        ];


        let medicalNumArrayItem = [];
        medicalNumArray.map((item, index) => {medicalNumArrayItem.push({key: index, label: item, value: index})})

        let medicalTimeArrayItem = [];
        medicalTimeArray.map((item, index) => {medicalTimeArrayItem.push({key: index, label: item, value: index})})

        let indivTypeArrayItem = [];
        indivTypeArray.map((item, index) => {indivTypeArrayItem.push({key: index, label: item, value: index})})

        let insurancerIndivTimeArrayItem = [];
        insurancerIndivTimeArray.map((item, index) => {insurancerIndivTimeArrayItem.push({key: index, label: item, value: index})})

        let insuranceTypeArrayItem = [];
        insuranceTypeArray.map((item, index) => {insuranceTypeArrayItem.push({key: index, label: item, value: index})})

        let travelArrayItem = [
            {key: 0, label: "انتخاب نشده", value: 0},
            {key: 1, label: "1 تا 7 روز", value: "1-7"},
            {key: 2, label: "8 تا 15 روز", value: "8-15"},
            {key: 3, label: "16 تا 23 روز", value: "16-23"},
            {key: 4, label: "24 تا 31 روز", value: "24-31"},
            {key: 5, label: "32 تا 45 روز", value: "32-45"},
            {key: 6, label: "46 تا 62 روز", value: "46-62"},
            {key: 7, label: "63 تا 92 روز", value: "63-92"},
            {key: 8, label: "6 ماه", value: "180"},
            {key: 9, label: "1 سال", value: "360"}
        ];

        let ageArrayItem = [];
        ageArray.map((item, index) => {ageArrayItem.push({key: index, label: item, value: index})})

        // const {posts, categories, user} = this.props;
        const text11 =  <Text style={{textAlign: 'right', fontFamily: 'IRANSansMobile(FaNum)'}} >1971</Text>
        if(this.state.loading){
            return (<Loader />)
        } else
        return (
            <SafeAreaView style={styles.container}>
                <View style={styles.header}>
                    <HomeHeader active={1} pageTitle={this.props.pageTitle} openDrawer={this.props.openDrawer}/>
                </View>
                <ScrollView style={styles.scroll}>
                    <View style={styles.bodyContainer}>
                        <View style={{width: '100%', zIndex: 0, paddingBottom: 70}}>
                            {
                                this.props.fire ?
                                    <View>
                                        <Text style={styles.text}>نوع ملک</Text>
                                        <View style={{position: 'relative', elevation: 5, zIndex: 3, width: '100%'}}>
                                            <FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: '45%', left: 10}}/>
                                            <SelectInput
                                                value={this.state.input1}
                                                options={typeArrayItem}
                                                onCancelEditing={() => console.log('onCancel')}
                                                submitKeyText="انتخاب"
                                                cancelKeyText="لغو"
                                                labelStyle={{color: 'rgb(150, 150, 150)', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13, paddingLeft: '78%'}}
                                                pickerItemsStyle={{fontFamily: 'IRANSansMobile(FaNum)', fontSize: 15}}
                                                buttonsTextStyle={{fontFamily: 'IRANSansMobile(FaNum)', fontSize: 15}}
                                                onSubmitEditing={(itemValue) =>{
                                                    console.log('itemValue for years', itemValue)
                                                    this.setState({
                                                        input1: itemValue
                                                    }, () => {
                                                        this.setState({input1Title: typeArray[itemValue]}, () => {console.log(this.state.input1Title)});
                                                        this.test();
                                                    })}}
                                                // style={{height: 42, paddingRight: 10, width: '100%', backgroundColor: 'white'}}
                                                style={{
                                                    flexDirection: 'row',
                                                    height: 42,
                                                    padding: 8,
                                                    marginTop: 16,
                                                    backgroundColor: '#FFFFFF',
                                                    borderRadius: 10, borderColor: this.state.redBorder && this.state.input1 === 0 ? 'red' : (this.state.input1 !== 0 ? 'rgb(50, 197, 117)' : 'transparent'), borderWidth: 1
                                                }}
                                            />
                                        </View>

                                        <Text style={styles.text}>نوع سازه</Text>
                                        <View style={{position: 'relative', elevation: 5, zIndex: 3, width: '100%'}}>
                                            <FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: '45%', left: 10}}/>
                                            <SelectInput
                                                value={this.state.input2}
                                                options={structureArrayItem}
                                                onCancelEditing={() => console.log('onCancel')}
                                                submitKeyText="انتخاب"
                                                cancelKeyText="لغو"
                                                labelStyle={{color: 'rgb(150, 150, 150)', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13, paddingLeft: '78%'}}
                                                pickerItemsStyle={{fontFamily: 'IRANSansMobile(FaNum)', fontSize: 15}}
                                                buttonsTextStyle={{fontFamily: 'IRANSansMobile(FaNum)', fontSize: 15}}
                                                onSubmitEditing={(itemValue) =>{
                                                    console.log('itemValue for years', itemValue)
                                                    this.setState({
                                                        input2: itemValue
                                                    }, () => {
                                                        let name = null;
                                                        structureArrayItem.map((item)=> item.value === itemValue ? name = item.label : null )
                                                        this.setState({input2Title: name}, () => {console.log(this.state.input2Title)});
                                                        this.test();
                                                    })}}
                                                // style={{height: 42, paddingRight: 10, width: '100%', backgroundColor: 'white'}}
                                                style={{
                                                    flexDirection: 'row',
                                                    height: 42,
                                                    padding: 8,
                                                    marginTop: 16,
                                                    backgroundColor: '#FFFFFF',
                                                    borderRadius: 10,borderColor: this.state.redBorder && this.state.input2 === 0 ? 'red' : (this.state.input2 !== 0 ? 'rgb(50, 197, 117)' : 'transparent'), borderWidth: 1
                                                }}
                                            />
                                        </View>

                                        <Text style={styles.text}>ارزش لوازم خانگی(تومان)</Text>

                                        <View style={{position: 'relative', elevation: 5, zIndex: 3, width: '100%', backgroundColor: 'white', height: 42, borderRadius: 10, borderColor: this.state.redBorder && this.state.inputTxt1 === '' ? 'red' : ( this.state.inputTxt1 !== '' ? 'rgb(50, 197, 117)' : 'transparent'), borderWidth: this.state.redBorder && this.state.inputTxt1 === '' ? 1 : ( this.state.inputTxt1.value !== '' ? 1 : 0)}}>
                                            <TextInput
                                                onChangeText={(text) =>{
                                                    this.setState({
                                                        inputTxt1: text.toString().replace(/,/g, "")
                                                    }, () => {
                                                        this.test();
                                                    })}}
                                                keyboardType={"numeric"}
                                                placeholder='ارزش لوازم خانگی'
                                                placeholderTextColor={'gray'}
                                                underlineColorAndroid='transparent'
                                                // value={this.state.inputTxt1}
                                                value={vaclePriceStr}
                                                style={{
                                                    height: 42,
                                                    paddingRight: 25,
                                                    width: '100%',
                                                    color: 'gray',
                                                    fontSize: 14,
                                                    textAlign: 'right',
                                                    // elevation: 4,
                                                    shadowColor: 'lightgray',
                                                    shadowOffset: {width: 5,height: 5},
                                                    shadowOpacity: 1
                                                }}
                                            />
                                        </View>

                                        <Text style={styles.text}>تعداد واحد</Text>
                                        <View style={{position: 'relative', elevation: 5, zIndex: 3, width: '100%', backgroundColor: 'white', height: 42, borderRadius: 10, borderColor: this.state.redBorder && this.state.inputTxt2 === '' ? 'red' : ( this.state.inputTxt2 !== '' ? 'rgb(50, 197, 117)' : 'transparent'), borderWidth: this.state.redBorder && this.state.inputTxt2 === '' ? 1 : ( this.state.inputTxt2 !== '' ? 1 : 0)}}>
                                            <TextInput
                                                // onFocus={() => {  this.setState({showPicker: true}); this.test();}}
                                                onChangeText={(text) =>{
                                                    this.setState({
                                                        inputTxt2: text
                                                    }, () => {
                                                        this.test();
                                                    })}}
                                                placeholder='تعداد واحد'
                                                keyboardType={"numeric"}
                                                placeholderTextColor={'gray'}
                                                underlineColorAndroid='transparent'
                                                value={this.state.inputTxt2}
                                                style={{
                                                    height: 42,
                                                    paddingRight: 25,
                                                    width: '100%',
                                                    color: 'gray',
                                                    fontSize: 14,
                                                    textAlign: 'right',
                                                    // elevation: 4,
                                                    shadowColor: 'lightgray',
                                                    shadowOffset: {width: 5,height: 5},
                                                    shadowOpacity: 1,
                                                }}
                                            />
                                        </View>
                                        <Text style={styles.text}>متراژ مورد بیمه</Text>
                                        <View style={{position: 'relative', elevation: 5, zIndex: 3, width: '100%', backgroundColor: 'white', height: 42, borderRadius: 10, borderColor: this.state.redBorder && this.state.inputTxt3 === '' ? 'red' : ( this.state.inputTxt3 !== '' ? 'rgb(50, 197, 117)' : 'transparent'), borderWidth: this.state.redBorder && this.state.inputTxt3 === '' ? 1 : ( this.state.inputTxt3 !== '' ? 1 : 0)}}>
                                            <TextInput
                                                // onFocus={() => {  this.setState({showPicker: true}); this.test();}}
                                                onChangeText={(text) =>{
                                                    this.setState({
                                                        inputTxt3: text
                                                    }, () => {
                                                        this.test();
                                                    })}}
                                                keyboardType={"numeric"}
                                                placeholder='متراژ مورد بیمه'
                                                placeholderTextColor={'gray'}
                                                underlineColorAndroid='transparent'
                                                value={this.state.inputTxt3}
                                                style={{
                                                    height: 42,
                                                    paddingRight: 25,
                                                    width: '100%',
                                                    color: 'gray',
                                                    fontSize: 14,
                                                    textAlign: 'right',
                                                    // elevation: 4,
                                                    shadowColor: 'lightgray',
                                                    shadowOffset: {width: 5,height: 5},
                                                    shadowOpacity: 1
                                                }}
                                            />
                                        </View>
                                        <Text style={styles.text}>هزینه ساخت هر متر مربع</Text>
                                        <Slider
                                            style={{
                                                flex: 1,
                                                marginLeft: 10,
                                                marginTop: 20,
                                                marginRight: 10,
                                                alignItems: "stretch",
                                                justifyContent: "center",
                                                // backgroundColor: '#00b3bc'
                                            }}
                                            minimumValue={1}
                                            maximumValue={2}
                                            maximumTrackTintColor="#00b3bc"
                                            // minimumTrackTintColor="red"
                                            step={0.1}
                                            value={this.state.area_price}
                                            onValueChange={value => this.setState({area_price: value, sliderChange: true}, () => {this.test();})}/>
                                        <Text style={{textAlign: 'center'}}>{this.state.area_price === 1.7000000000000002 ? 1.7 : this.state.area_price}</Text>
                                        <Text style={styles.featureLabel}>پوشش های اجباری</Text>
                                        <SwitchRow label="آتش سوزی" switchButtons={() => null} on />
                                        <SwitchRow label="صاعقه" switchButtons={() => null} on />
                                        <SwitchRow label="انفجار" switchButtons={() => null} on />
                                        <Text style={styles.featureLabel}>پوشش های اختیاری</Text>
                                        <SwitchRow label="زلزله"  switchButtons={(status) => this.switchButtons(status, 1, 'earthquake')} />
                                        <SwitchRow label="ترکیدگی لوله"  switchButtons={(status) => this.switchButtons(status, 2, 'pipe')} />
                                        <SwitchRow label="نشست زمین"  switchButtons={(status) => this.switchButtons(status, 3, 'earth')} />
                                        <SwitchRow label="ضایعات ناشی از برف و باران"  switchButtons={(status) => this.switchButtons(status, 4, 'rain')} />
                                        <SwitchRow label="طوفان"  switchButtons={(status) => this.switchButtons(status, 5, 'tornado')} />
                                        <SwitchRow label="سیل"  switchButtons={(status) => this.switchButtons(status, 6, 'flood')} />
                                        <SwitchRow label="سقوط هواپیما"  switchButtons={(status) => this.switchButtons(status, 7, 'airplane')} />
                                        <SwitchRow label="سرقت مربوط به شکست حرز"  switchButtons={(status) => this.switchButtons(status, 8, 'steal')} />
                                    </View> : null
                            }
                            {
                                this.props.travel ?
                                    <View>
                                        <Text style={styles.text}>کشور</Text>
                                        <TouchableOpacity onPress={this.onShowinput1} style={{position: 'relative', elevation: 5, zIndex: 3, width: '100%', backgroundColor: 'white', height: 42,  borderColor: this.state.redBorder && this.state.input1 === 0 ? 'red' : ( this.state.input1 !== 0 ? 'rgb(50, 197, 117)' : 'transparent'), borderWidth: 1, borderRadius: 10}}>
                                            <FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: 10, left: 10}}/>
                                            <Text style={{paddingTop: 7, paddingRight: 10, textAlign: 'right', fontFamily: 'IRANSansMobile(FaNum)', color: 'gray' }}>{this.state.loading ? "در حال بارگذاری..." : this.state.input1 === 0 ? "انتخاب نشده" : this.state.input1Title}</Text>
                                            <ModalFilterPicker
                                                visible={this.state.visibleinput1}
                                                onSelect={this.onSelectInput1}
                                                onCancel={this.onCancelinput1}
                                                options={ this.state.countries.map((item) => {return {key: item.id, label: item.title, value: item.id}})}
                                                filterTextInputContainerStyle={{padding: 15, borderBottomWidth: 1, borderBottomColor: 'gray'}}
                                                placeholderText="جستجو ..."
                                                cancelButtonText="لغو"
                                                filterTextInputStyle={{textAlign: 'right'}}
                                                optionTextStyle={{textAlign: 'right', width: '100%'}}
                                                titleTextStyle={{textAlign: 'right'}}
                                            />
                                        </TouchableOpacity>

                                        <Text style={styles.text}>نوع بیمه</Text>
                                        <View style={{position: 'relative', elevation: 5, zIndex: 3, width: '100%'}}>
                                            <FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: '45%', left: 10}}/>
                                            <SelectInput
                                                value={this.state.input2}
                                                options={insuranceTypeArrayItem}
                                                onCancelEditing={() => console.log('onCancel')}
                                                submitKeyText="انتخاب"
                                                cancelKeyText="لغو"
                                                labelStyle={{color: 'rgb(150, 150, 150)', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13, paddingLeft: '78%'}}
                                                pickerItemsStyle={{fontFamily: 'IRANSansMobile(FaNum)', fontSize: 15}}
                                                buttonsTextStyle={{fontFamily: 'IRANSansMobile(FaNum)', fontSize: 15}}
                                                onSubmitEditing={(itemValue) =>{
                                                    console.log('itemValue for years', itemValue)
                                                    this.setState({
                                                        input2: itemValue
                                                    }, () => {
                                                        this.setState({input2Title: insuranceTypeArray[itemValue]}, () => {console.log(this.state.input2Title)});
                                                        this.test();
                                                    })}}
                                                style={{
                                                    flexDirection: 'row',
                                                    height: 42,
                                                    padding: 8,
                                                    marginTop: 16,
                                                    backgroundColor: '#FFFFFF',
                                                    borderRadius: 10, borderColor: this.state.redBorder && this.state.input2 === 0 ? 'red' : (this.state.input2 !== 0 ? 'rgb(50, 197, 117)' : 'transparent'), borderWidth: 1
                                                }}
                                            />
                                        </View>

                                        <Text style={styles.text}>مدت اقامت</Text>

                                        <View style={{position: 'relative', elevation: 5, zIndex: 3, width: '100%'}}>
                                            <FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: '45%', left: 10}}/>
                                            <SelectInput
                                                value={this.state.input3}
                                                options={travelArrayItem}
                                                onCancelEditing={() => console.log('onCancel')}
                                                submitKeyText="انتخاب"
                                                cancelKeyText="لغو"
                                                labelStyle={{color: 'rgb(150, 150, 150)', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13, paddingLeft: '78%'}}
                                                pickerItemsStyle={{fontFamily: 'IRANSansMobile(FaNum)', fontSize: 15}}
                                                buttonsTextStyle={{fontFamily: 'IRANSansMobile(FaNum)', fontSize: 15}}
                                                onSubmitEditing={(itemValue) =>{
                                                    console.log('itemValue for years', itemValue)
                                                    this.setState({
                                                        input3: itemValue
                                                    }, () => {
                                                        let name = null;
                                                        travelArrayItem.map((item)=> item.value === itemValue ? name = item.label : null )
                                                        this.setState({input3Title: name}, () => {console.log(this.state.input3Title)});
                                                        this.test();
                                                    })}}
                                                style={{
                                                    flexDirection: 'row',
                                                    height: 42,
                                                    padding: 8,
                                                    marginTop: 16,
                                                    backgroundColor: '#FFFFFF',
                                                    borderRadius: 10, borderColor: this.state.redBorder && this.state.input3 === 0 ? 'red' : (this.state.input3 !== 0 ? 'rgb(50, 197, 117)' : 'transparent'), borderWidth: 1
                                                }}
                                            />
                                        </View>

                                        <Text style={styles.text}>سن</Text>

                                        <View style={{position: 'relative', elevation: 5, zIndex: 3, width: '100%'}}>
                                            <FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: '45%', left: 10}}/>
                                            <SelectInput
                                                value={this.state.age}
                                                options={ageArrayItem}
                                                onCancelEditing={() => console.log('onCancel')}
                                                submitKeyText="انتخاب"
                                                cancelKeyText="لغو"
                                                labelStyle={{color: 'rgb(150, 150, 150)', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13, paddingLeft: '78%'}}
                                                pickerItemsStyle={{fontFamily: 'IRANSansMobile(FaNum)', fontSize: 15}}
                                                buttonsTextStyle={{fontFamily: 'IRANSansMobile(FaNum)', fontSize: 15}}
                                                onSubmitEditing={(itemValue) =>{
                                                    console.log('itemValue for years', itemValue)
                                                    this.setState({
                                                        age: itemValue
                                                    }, () => {
                                                        this.setState({ageTitle: ageArray[itemValue]}, () => {console.log(this.state.ageTitle)});
                                                        this.test();
                                                    })}}
                                                style={{
                                                    flexDirection: 'row',
                                                    height: 42,
                                                    padding: 8,
                                                    marginTop: 16,
                                                    backgroundColor: '#FFFFFF',
                                                    borderRadius: 10, borderColor: this.state.redBorder && this.state.age === 0 ? 'red' : (this.state.age !== 0 ? 'rgb(50, 197, 117)' : 'transparent'), borderWidth: 1
                                                }}
                                            />
                                        </View>

                                    </View>: null
                            }
                            {
                                this.props.medical ?
                                    <View>

                                        <Text style={styles.text}>نوع بیمه</Text>
                                        <View style={{position: 'relative', elevation: 5, zIndex: 3, width: '100%'}}>
                                            <FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: '45%', left: 10}}/>
                                            <SelectInput
                                                value={this.state.input2}
                                                options={[{key: 0, label: "انتخاب نشده", value: "0" },{key: 1, label: "پزشکی", value: "1"}, {key: 2, label: "پیراپزشکی", value: "2"}]}
                                                onCancelEditing={() => console.log('onCancel')}
                                                submitKeyText="انتخاب"
                                                cancelKeyText="لغو"
                                                labelStyle={{color: 'rgb(150, 150, 150)', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13, paddingLeft: '78%'}}
                                                pickerItemsStyle={{fontFamily: 'IRANSansMobile(FaNum)', fontSize: 15}}
                                                buttonsTextStyle={{fontFamily: 'IRANSansMobile(FaNum)', fontSize: 15}}
                                                onSubmitEditing={(itemValue) =>{
                                                    console.log('itemValue for years', itemValue)
                                                    this.setState({
                                                        input2: itemValue
                                                    }, () => {
                                                    if(itemValue !== '0') {
                                                        this.getExperts();
                                                    }
                                                    this.test();
                                                })}}
                                                style={{
                                                    flexDirection: 'row',
                                                    height: 42,
                                                    padding: 8,
                                                    marginTop: 16,
                                                    backgroundColor: '#FFFFFF',
                                                    borderRadius: 10, borderColor: this.state.redBorder && this.state.input2 === '0' ? 'red' : (this.state.input2 !== '0' ? 'rgb(50, 197, 117)' : 'transparent'), borderWidth: 1
                                                }}
                                            />
                                        </View>


                                        <Text style={styles.text}>نوع تخصص</Text>
                                        <TouchableOpacity onPress={this.onShowinput2} style={{position: 'relative', elevation: 5, zIndex: 3, width: '100%', backgroundColor: 'white', height: 42,  borderColor: this.state.redBorder && this.state.input1 === 0 ? 'red' : ( this.state.input1 !== 0 ? 'rgb(50, 197, 117)' : 'transparent'), borderWidth: 1, borderRadius: 10}}>
                                            <FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: 10, left: 10}}/>
                                            <Text style={{paddingTop: 7, paddingRight: 10,  fontFamily: 'IRANSansMobile(FaNum)',textAlign: 'right', color: 'gray' }}>{this.state.loading2 ? "در حال بارگذاری..." : this.state.input1 === 0 ? "انتخاب نشده" : this.state.input1Title}</Text>
                                            <ModalFilterPicker
                                                visible={this.state.visibleinput2}
                                                onSelect={this.onSelectInput2}
                                                onCancel={this.onCancelinput2}
                                                options={ this.state.expert.map((item) => {return {key: item.id, label: item.title, value: item.id}})}
                                                filterTextInputContainerStyle={{padding: 15, borderBottomWidth: 1, borderBottomColor: 'gray'}}
                                                placeholderText="جستجو ..."
                                                cancelButtonText="لغو"
                                                filterTextInputStyle={{textAlign: 'right'}}
                                                optionTextStyle={{textAlign: 'right', width: '100%'}}
                                                titleTextStyle={{textAlign: 'right'}}
                                            />
                                        </TouchableOpacity>



                                        <Text style={styles.text}>تعداد دیه</Text>
                                        <View style={{position: 'relative', elevation: 5, zIndex: 3, width: '100%'}}>
                                            <FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: '45%', left: 10}}/>
                                            <SelectInput
                                                value={this.state.input3}
                                                options={medicalNumArrayItem}
                                                onCancelEditing={() => console.log('onCancel')}
                                                submitKeyText="انتخاب"
                                                cancelKeyText="لغو"
                                                labelStyle={{color: 'rgb(150, 150, 150)', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13, paddingLeft: '78%'}}
                                                pickerItemsStyle={{fontFamily: 'IRANSansMobile(FaNum)', fontSize: 15}}
                                                buttonsTextStyle={{fontFamily: 'IRANSansMobile(FaNum)', fontSize: 15}}
                                                onSubmitEditing={(itemValue) =>{
                                                    console.log('itemValue for years', itemValue)
                                                    this.setState({
                                                        input3: itemValue
                                                    }, () => {
                                                        this.setState({input3Title: medicalNumArray[itemValue]}, () => {console.log(this.state.input3Title)});
                                                        this.test();
                                                    })}}
                                                style={{
                                                    flexDirection: 'row',
                                                    height: 42,
                                                    padding: 8,
                                                    marginTop: 16,
                                                    backgroundColor: '#FFFFFF',
                                                    borderRadius: 10, borderColor: this.state.redBorder && this.state.input3 === 0 ? 'red' : (this.state.input3 !== 0 ? 'rgb(50, 197, 117)' : 'transparent'), borderWidth: 1
                                                }}
                                            />
                                        </View>


                                        <Text style={styles.text}>مدت بیمه نامه</Text>


                                        <View style={{position: 'relative', elevation: 5, zIndex: 3, width: '100%'}}>
                                            <FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: '45%', left: 10}}/>
                                            <SelectInput
                                                value={this.state.input4}
                                                options={medicalTimeArrayItem}
                                                onCancelEditing={() => console.log('onCancel')}
                                                submitKeyText="انتخاب"
                                                cancelKeyText="لغو"
                                                labelStyle={{color: 'rgb(150, 150, 150)', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13, paddingLeft: '78%'}}
                                                pickerItemsStyle={{fontFamily: 'IRANSansMobile(FaNum)', fontSize: 15}}
                                                buttonsTextStyle={{fontFamily: 'IRANSansMobile(FaNum)', fontSize: 15}}
                                                onSubmitEditing={(itemValue) =>{
                                                    console.log('itemValue for years', itemValue)
                                                    this.setState({
                                                        input4: itemValue
                                                    }, () => {
                                                        this.setState({input4Title: medicalTimeArray[itemValue]}, () => {console.log(this.state.input4Title)});
                                                        this.test();
                                                    })}}
                                                style={{
                                                    flexDirection: 'row',
                                                    height: 42,
                                                    padding: 8,
                                                    marginTop: 16,
                                                    backgroundColor: '#FFFFFF',
                                                    borderRadius: 10, borderColor: this.state.redBorder && this.state.input4 === 0 ? 'red' : (this.state.input4 !== 0 ? 'rgb(50, 197, 117)' : 'transparent'), borderWidth: 1
                                                }}
                                            />
                                        </View>
                                    </View>: null
                            }
                            {
                                this.props.individual ?
                                    <View>
                                        <Text style={styles.text}>بیمه گر پایه</Text>

                                        <View style={{position: 'relative', elevation: 5, zIndex: 3, width: '100%'}}>
                                            <FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: '45%', left: 10}}/>
                                            <SelectInput
                                                value={this.state.input4}
                                                options={indivTypeArrayItem}
                                                onCancelEditing={() => console.log('onCancel')}
                                                submitKeyText="انتخاب"
                                                cancelKeyText="لغو"
                                                labelStyle={{color: 'rgb(150, 150, 150)', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13, paddingLeft: '78%'}}
                                                pickerItemsStyle={{fontFamily: 'IRANSansMobile(FaNum)', fontSize: 15}}
                                                buttonsTextStyle={{fontFamily: 'IRANSansMobile(FaNum)', fontSize: 15}}
                                                onSubmitEditing={(itemValue) =>{
                                                    console.log('itemValue for years', itemValue)
                                                    this.setState({
                                                        input4: itemValue
                                                    }, () => {
                                                        this.setState({input4Title: indivTypeArray[itemValue]}, () => {console.log(this.state.input4Title)});
                                                        this.test();
                                                    })}}
                                                style={{
                                                    flexDirection: 'row',
                                                    height: 42,
                                                    padding: 8,
                                                    marginTop: 16,
                                                    backgroundColor: '#FFFFFF',
                                                    borderRadius: 10, borderColor: this.state.redBorder && this.state.input4 === 0 ? 'red' : (this.state.input4 !== 0 ? 'rgb(50, 197, 117)' : 'transparent'), borderWidth: 1
                                                }}
                                            />
                                        </View>

                                        <Text style={styles.text}>نوع بیمه</Text>

                                        <View style={{position: 'relative', elevation: 5, zIndex: 3, width: '100%'}}>
                                            <FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: '45%', left: 10}}/>
                                            <SelectInput
                                                value={this.state.input1}
                                                options={insurancerIndivTimeArrayItem}
                                                onCancelEditing={() => console.log('onCancel')}
                                                submitKeyText="انتخاب"
                                                cancelKeyText="لغو"
                                                labelStyle={{color: 'rgb(150, 150, 150)', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13, paddingLeft: '78%'}}
                                                pickerItemsStyle={{fontFamily: 'IRANSansMobile(FaNum)', fontSize: 15}}
                                                buttonsTextStyle={{fontFamily: 'IRANSansMobile(FaNum)', fontSize: 15}}
                                                onSubmitEditing={(itemValue) =>{
                                                    console.log('itemValue for years', itemValue)
                                                    this.setState({
                                                        input1: itemValue
                                                    }, () => {
                                                        this.setState({input1Title: insurancerIndivTimeArray[itemValue]}, () => {console.log(this.state.input1Title)});
                                                        this.test();
                                                    })}}
                                                style={{
                                                    flexDirection: 'row',
                                                    height: 42,
                                                    padding: 8,
                                                    marginTop: 16,
                                                    backgroundColor: '#FFFFFF',
                                                    borderRadius: 10, borderColor: this.state.redBorder && this.state.input1 === 0 ? 'red' : (this.state.input1 !== 0 ? 'rgb(50, 197, 117)' : 'transparent'), borderWidth: 1
                                                }}
                                            />
                                        </View>

                                        {
                                            this.state.input1 === 2 ?
                                                <View>
                                                    <Text style={styles.text}>پوشش دندان پزشکی</Text>

                                                    <View style={{position: 'relative', elevation: 5, zIndex: 3, width: '100%'}}>
                                                        <FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: '45%', left: 10}}/>
                                                        <SelectInput
                                                            value={this.state.input2}
                                                            options={[ {key: 0, label: "انتخاب نشده", value: 0},  {key: 1, label: "بله", value: 1}, {key: 2, label: "خیر", value: 2}]}
                                                            onCancelEditing={() => console.log('onCancel')}
                                                            submitKeyText="انتخاب"
                                                            cancelKeyText="لغو"
                                                            labelStyle={{color: 'rgb(150, 150, 150)', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13, paddingLeft: '78%'}}
                                                            pickerItemsStyle={{fontFamily: 'IRANSansMobile(FaNum)', fontSize: 15}}
                                                            buttonsTextStyle={{fontFamily: 'IRANSansMobile(FaNum)', fontSize: 15}}
                                                            onSubmitEditing={(itemValue) =>{
                                                                console.log('itemValue for years', itemValue)
                                                                this.setState({
                                                                    input2: itemValue
                                                                }, () => {
                                                                    this.test();
                                                                })}}
                                                            style={{
                                                                flexDirection: 'row',
                                                                height: 42,
                                                                padding: 8,
                                                                marginTop: 16,
                                                                backgroundColor: '#FFFFFF',
                                                                borderRadius: 10, borderColor: this.state.redBorder && this.state.input2 === 0 ? 'red' : (this.state.input2 !== 0 ? 'rgb(50, 197, 117)' : 'transparent'), borderWidth: 1
                                                            }}
                                                        />
                                                    </View>

                                                    <Text style={styles.text}>سن</Text>

                                                    <View style={{position: 'relative', elevation: 5, zIndex: 3, width: '100%'}}>
                                                        <FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: '45%', left: 10}}/>
                                                        <SelectInput
                                                            value={this.state.age}
                                                            options={[
                                                                {key: 0, label: "انتخاب نشده", value: 7},
                                                                {key: 1, label: "0-15", value: 0},
                                                                {key: 2, label: "16-50", value: 1},
                                                                {key: 3, label: "51-60", value: 2},
                                                                {key: 4, label: "61-70", value: 3}
                                                            ]}
                                                            onCancelEditing={() => console.log('onCancel')}
                                                            submitKeyText="انتخاب"
                                                            cancelKeyText="لغو"
                                                            labelStyle={{color: 'rgb(150, 150, 150)', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13, paddingLeft: '78%'}}
                                                            pickerItemsStyle={{fontFamily: 'IRANSansMobile(FaNum)', fontSize: 15}}
                                                            buttonsTextStyle={{fontFamily: 'IRANSansMobile(FaNum)', fontSize: 15}}
                                                            onSubmitEditing={(itemValue) =>{
                                                                console.log('itemValue for years', itemValue)
                                                                this.setState({
                                                                    age: itemValue
                                                                }, () => {
                                                                    this.test();
                                                                })}}
                                                            style={{
                                                                flexDirection: 'row',
                                                                height: 42,
                                                                padding: 8,
                                                                marginTop: 16,
                                                                backgroundColor: '#FFFFFF',
                                                                borderRadius: 10, borderColor: this.state.redBorder && this.state.age === 7 ? 'red' : (this.state.age !== 7 ? 'rgb(50, 197, 117)' : 'transparent'), borderWidth: 1
                                                            }}
                                                        />
                                                    </View>
                                                </View> : null
                                        }
                                        {
                                            this.state.input1 === 1 ?
                                                <View>
                                                    <Text style={styles.text}>تعداد نفرات</Text>
                                                    <View style={{position: 'relative', elevation: 5, zIndex: 3, width: '100%', backgroundColor: 'white', height: 42, borderRadius: 10, borderColor: this.state.redBorder && this.state.inputTxt1 === '' ? 'red' : ( this.state.inputTxt1 !== '' ? 'rgb(50, 197, 117)' : 'transparent'), borderWidth: 1}}>
                                                        <TextInput
                                                            // onFocus={() => {  this.setState({showPicker: true}); this.test();}}
                                                            onChangeText={(text) =>{
                                                                this.setState({
                                                                    inputTxt1: text
                                                                }, () => {
                                                                    this.test();
                                                                })}}
                                                            placeholder='تعداد نفرات'
                                                            placeholderTextColor={'gray'}
                                                            underlineColorAndroid='transparent'
                                                            value={this.state.inputTxt1}
                                                            keyboardType='numeric'
                                                            style={{
                                                                height: 40,
                                                                paddingRight: 25,
                                                                width: '100%',
                                                                color: 'gray',
                                                                fontSize: 14,
                                                                textAlign: 'right',
                                                                // elevation: 4,
                                                                shadowColor: 'lightgray',
                                                                shadowOffset: {width: 5,height: 5},
                                                                shadowOpacity: 1 ,
                                                            }}
                                                        />
                                                    </View>
                                                </View> : null
                                        }
                                    </View>: null
                            }
                            {
                                this.props.age ?
                                    <View>
                                        <Text style={styles.text}>حق بیمه</Text>


                                        <View style={{position: 'relative', elevation: 5, zIndex: 3, width: '100%'}}>
                                            <FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: '45%', left: 10}}/>
                                            <SelectInput
                                                value={this.state.input1}
                                                options={payArrayItem}
                                                onCancelEditing={() => console.log('onCancel')}
                                                submitKeyText="انتخاب"
                                                cancelKeyText="لغو"
                                                labelStyle={{color: 'rgb(150, 150, 150)', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13, paddingLeft: '78%'}}
                                                pickerItemsStyle={{fontFamily: 'IRANSansMobile(FaNum)', fontSize: 15}}
                                                buttonsTextStyle={{fontFamily: 'IRANSansMobile(FaNum)', fontSize: 15}}
                                                onSubmitEditing={(itemValue) =>{
                                                    console.log('itemValue for years', itemValue)
                                                    this.setState({
                                                        input1: itemValue
                                                    }, () => {
                                                        this.setState({input1Title: payArray[itemValue]}, () => {console.log(this.state.input1Title)});
                                                        this.test();
                                                    })}}
                                                style={{
                                                    flexDirection: 'row',
                                                    height: 42,
                                                    padding: 8,
                                                    marginTop: 16,
                                                    backgroundColor: '#FFFFFF',
                                                    borderRadius: 10, borderColor: this.state.redBorder && this.state.input1 === 0 ? 'red' : (this.state.input1 !== 0 ? 'rgb(50, 197, 117)' : 'transparent'), borderWidth: 1
                                                }}
                                            />
                                        </View>

                                        <Text style={styles.text}>نحوه پرداخت</Text>
                                        <View style={{position: 'relative', elevation: 5, zIndex: 3, width: '100%'}}>
                                            <FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: '45%', left: 10}}/>
                                            <SelectInput
                                                value={this.state.input2}
                                                options={costArrayItem}
                                                onCancelEditing={() => console.log('onCancel')}
                                                submitKeyText="انتخاب"
                                                cancelKeyText="لغو"
                                                labelStyle={{color: 'rgb(150, 150, 150)', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13, paddingLeft: '78%'}}
                                                pickerItemsStyle={{fontFamily: 'IRANSansMobile(FaNum)', fontSize: 15}}
                                                buttonsTextStyle={{fontFamily: 'IRANSansMobile(FaNum)', fontSize: 15}}
                                                onSubmitEditing={(itemValue) =>{
                                                    console.log('itemValue for years', itemValue)
                                                    this.setState({
                                                        input2: itemValue
                                                    }, () => {
                                                        this.setState({input2Title: costArray[itemValue]}, () => {console.log(this.state.input2Title)});
                                                        this.test();
                                                    })}}
                                                style={{
                                                    flexDirection: 'row',
                                                    height: 42,
                                                    padding: 8,
                                                    marginTop: 16,
                                                    backgroundColor: '#FFFFFF',
                                                    borderRadius: 10, borderColor: this.state.redBorder && this.state.input2 === 0 ? 'red' : (this.state.input2 !== 0 ? 'rgb(50, 197, 117)' : 'transparent'), borderWidth: 1
                                                }}
                                            />
                                        </View>

                                        <Text style={styles.text}>طول مدت قرارداد</Text>


                                        <View style={{position: 'relative', elevation: 5, zIndex: 3, width: '100%'}}>
                                            <FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: '45%', left: 10}}/>
                                            <SelectInput
                                                value={this.state.input3}
                                                options={yearArrayItem}
                                                onCancelEditing={() => console.log('onCancel')}
                                                submitKeyText="انتخاب"
                                                cancelKeyText="لغو"
                                                labelStyle={{color: 'rgb(150, 150, 150)', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13, paddingLeft: '78%'}}
                                                pickerItemsStyle={{fontFamily: 'IRANSansMobile(FaNum)', fontSize: 15}}
                                                buttonsTextStyle={{fontFamily: 'IRANSansMobile(FaNum)', fontSize: 15}}
                                                onSubmitEditing={(itemValue) =>{

                                                    console.log('itemValue for years', itemValue)
                                                    this.setState({
                                                        input3: itemValue
                                                    }, () => {
                                                        let name = null;
                                                        yearArrayItem.map((item)=> item.value === itemValue ? name = item.label : null )
                                                        this.setState({input3Title: name}, () => {console.log(this.state.input3Title)});
                                                        this.test();
                                                    })}}
                                                style={{
                                                    flexDirection: 'row',
                                                    height: 42,
                                                    padding: 8,
                                                    marginTop: 16,
                                                    backgroundColor: '#FFFFFF',
                                                    borderRadius: 10, borderColor: this.state.redBorder && this.state.input3 === 0 ? 'red' : (this.state.input3 !== 0 ? 'rgb(50, 197, 117)' : 'transparent'), borderWidth: 1
                                                }}
                                            />
                                        </View>

                                    </View>: null
                            }
                        </View>
                        <AlertView
                            closeModal={(title) => this.closeModal(title)}
                            modalVisible={this.state.modalVisible}
                            onChange={(visible) => this.setModalVisible(visible)}

                            title='مشکلی در برقراری ارتباط با سرور به وجود آمده لطفا دوباره سعی کنید'
                        />
                    </View>
                </ScrollView>
                <View style={styles.footer}>
                    <TouchableOpacity style={[styles.iconLeftContainer, {backgroundColor: this.state.nextStep ? 'rgba(255, 193, 39, 1)' : 'rgba(200, 200, 200, 1)' }]} onPress={() => this.nextStep()}>
                        <FIcon name="arrow-left" size={18} color="white" style={{borderColor: 'white', borderWidth: 1, borderRadius: 20, padding: 3, marginRight: 5}} />
                        <Text style={styles.label}>بعدی</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.iconRightContainer} onPress={() => this.onBackPress()}>
                        <IIcon name="md-close" size={22} color="rgba(17, 103, 253, 1)" />
                    </TouchableOpacity>
                </View>
                <LinearGradient  start={{x: 0, y: 0}} end={{x: 1, y: 1}} colors={['rgba(60, 177, 232, 1)', 'rgba(62, 64, 219, 1)']} style={styles.topBlue} />
            </SafeAreaView>
        );
    }
}
export default OtherVacleInfo;

